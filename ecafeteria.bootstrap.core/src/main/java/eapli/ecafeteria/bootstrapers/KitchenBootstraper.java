/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.authz.KitchenAlertsManagementController;
import eapli.ecafeteria.domain.kitchen.KitchenAlertManagement;
import eapli.ecafeteria.persistence.AlertRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
// * @author 1150425 & 1150482
 */
public class KitchenBootstraper implements Action {

    @Override
    public boolean execute() {
        final AlertRepository repo = PersistenceContext.repositories().AlertRepository();
        final KitchenAlertManagement kam = new KitchenAlertManagement("Kitchen Alert", 50, 75);
        
        try {
            repo.save(kam);
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            Logger.getLogger(KitchenBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

}
