package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.meals.PublishMealController;
import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.domain.kitchen.Material;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 1150416
 */
public class MealsBootstraper implements Action {

    @Override
    public boolean execute() {
        final DishRepository dishRepoUnit = PersistenceContext.repositories().dishes();
        final ArrayList<Dish> dishes = (ArrayList<Dish>) dishRepoUnit.allActiveDishes();

        for (Dish d : dishes) {
            register(MealType.LUNCH, d, "01-01-2018");
        }
        for (Dish d : dishes) {
            register(MealType.DINNER, d, "01-01-2018");
        }
        return false;
    }

    private void register(MealType mealType, Dish dish, String date) {
        final PublishMealController mealController = new PublishMealController();
        Meal m;
        try {
            m = mealController.publishMeal(mealController.buildMeal(mealType, dish, date));
        } catch (final DataConcurrencyException | DataIntegrityViolationException e) {
            System.out.println("Error creating meals!");
        }
    }

}
