package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.booking.BookingMealController;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.persistence.repositories.TransactionalContext;
import eapli.util.DateTime;
import java.util.List;

/**
 *
 * @author 1150416
 */
public class BookingsBootstraper implements Action {

    final TransactionalContext txCtx = PersistenceContext.repositories().buildTransactionalContext();
    final CafeteriaUserRepository cafeteriaUserRepoUnit = PersistenceContext.repositories().cafeteriaUsers(txCtx);
    final MealRepository mealRepoUnit = PersistenceContext.repositories().meals();

    @Override
    public boolean execute() {
        final CafeteriaUser jhon = cafeteriaUserRepoUnit.findByMecanographicNumber(new MecanographicNumber("900330"));
        final CafeteriaUser mary = cafeteriaUserRepoUnit.findByMecanographicNumber(new MecanographicNumber("900331"));
        final CafeteriaUser stan = cafeteriaUserRepoUnit.findByMecanographicNumber(new MecanographicNumber("900332"));

        List<Meal> lstMeal = (List<Meal>) mealRepoUnit.mealsByDateAndType(DateTime.parseDate("01-01-2018"),
                MealType.mealTypeByHour(DateTime.parseDate("01-01-2018")));

        Meal meal = lstMeal.get(0);

        register(jhon, meal);
        register(mary, meal);
        register(stan, meal);
        return false;
    }

    private void register(CafeteriaUser user, Meal meal) {
        final BookingMealController bmcont = new BookingMealController();

        try {
            bmcont.setCurrentUser(user);
            bmcont.bookMeal(meal);
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            System.out.println("Error Creating the bookings");
        }
    }
}
