/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.domain.delivery.Cashier;
import eapli.ecafeteria.persistence.CashierRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pedro
 */
public class CashierBootstraper  implements Action{

     @Override
    public boolean execute() {
        final CashierRepository cashierTypeRepo = PersistenceContext.repositories().cashier();
        final Cashier cashier = new Cashier();
        
        try {
            cashierTypeRepo.save(cashier);
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(CashierBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(CashierBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
    
}
