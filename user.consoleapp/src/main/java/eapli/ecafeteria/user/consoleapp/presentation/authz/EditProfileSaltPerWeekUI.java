package eapli.ecafeteria.user.consoleapp.presentation.authz;

import eapli.ecafeteria.application.authz.EditProfileSaltPerWeekController;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Nuno
 */
public class EditProfileSaltPerWeekUI extends AbstractUI {

    private final EditProfileSaltPerWeekController theController = new EditProfileSaltPerWeekController();

    @Override
    protected boolean doShow() {

        try {
            if (theController.changeSaltQuantWeek(Console.readInteger("Introduce the desired salt intake per week.")) != null) {
                System.out.println("Profile successfully updated.");
            }
        } catch (final DataConcurrencyException ex) {
            System.out.println("It wasn't possible to make any alteration, since this profile has changed since it was read.");
        } catch (final DataIntegrityViolationException ex) {
            System.out.println("The inserted data is not valid.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Edit Salt Intake Per Week.";
    }

}
