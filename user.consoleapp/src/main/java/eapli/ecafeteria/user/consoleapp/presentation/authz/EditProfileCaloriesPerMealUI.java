/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.authz;

import eapli.ecafeteria.application.authz.EditProfileCaloriesPerMealController;
import eapli.ecafeteria.user.consoleapp.presentation.ShowBalance;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;

/**
 *
 * @author Nuno
 */
public class EditProfileCaloriesPerMealUI extends AbstractUI implements ShowBalance {

    private final EditProfileCaloriesPerMealController theController = new EditProfileCaloriesPerMealController();

    @Override
    protected boolean doShow() {
        
        showBalance();

        try {
            if (theController.changeCalQuantMeal(Console.readInteger("Introduce the desired calories intake per meal.")) != null) {
                System.out.println("Profile successfully uptdated.");
            }
        } catch (final DataConcurrencyException ex) {
            System.out.println("It wasn't possible to make any alteration, since this profile has changed since it was read.");
        } catch (final DataIntegrityViolationException ex) {
            System.out.println("The inserted data is not valid.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Edit Salt Intake Per Meal.";
    }

}
