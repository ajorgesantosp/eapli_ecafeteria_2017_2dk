/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.bookingMeal;

import eapli.framework.actions.Action;

/**
 *
 * @author César Seabra & Fábio Correia
 */
public class BookingMealCancelAction implements Action{

    @Override
    public boolean execute() {
        return new BookingMealCancelUI().show();
    }
    
}
