/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.BalanceController;

/**
 *
 * @author Esquilo
 */
public interface ShowBalance   {
     
    
    default void showBalance() {
          BalanceController Bcontroller= new BalanceController();
        
         Bcontroller.getUser();

       
        System.out.println( "eCAFETERIA [@" + Application.session().session().authenticatedUser().id() + "]   " + "CURRENT BALANCE OF YOUR USERCARD: " + Bcontroller.getBalance());
        
}
     
     
 }
    
    

