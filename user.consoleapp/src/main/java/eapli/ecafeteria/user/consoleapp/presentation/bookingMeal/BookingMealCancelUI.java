/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.bookingMeal;

import eapli.ecafeteria.application.booking.BookingMealCancelController;
import eapli.ecafeteria.domain.booking.Booking;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author César Seabra & Fábio Correia
 */
public class BookingMealCancelUI extends AbstractUI {

    private final BookingMealCancelController theController = new BookingMealCancelController();

    /**
     * Toreturn the atual controller (BookingMealController).
     *
     * @return the actual controller
     */
    protected Controller controller() {
        return this.theController;
    }

    /**
     * Method that shows the UI to the user when called.
     *
     * @return the UI
     */
    @Override
    protected boolean doShow() {

        boolean loop = true;
        Iterable<Booking> bookings = theController.getReservedBookingsOfUser();

        final SelectWidget<Booking> selector = new SelectWidget<>("Bookings:", bookings, new BookingPrinter());

        selector.show();

        final Booking selectedBooking = selector.selectedElement();

        if (selectedBooking == null) {
            return true;
        }

        boolean refundTotalMoney = theController.validateHourToRefundMoney(selectedBooking);

        if (refundTotalMoney) {
            System.out.println("You will receive the total price of the booking!");
        } else {
            System.out.println("Since the hour is passing 10h(Lunch) or 16h(Dinner) you will only receive half the price of the booking!\n");
        }

        while (loop) {
            String res = Console.readLine("You want to confirm the cancelation of the selected booking?(Y (YES) or N (NO)");
            if (res.equalsIgnoreCase("y") || res.equalsIgnoreCase("yes")) {
                try {
                    theController.changeBookingState(refundTotalMoney);
                    loop = false;
                } catch (DataIntegrityViolationException | DataConcurrencyException ex) {
                    Logger.getLogger(BookingMealCancelUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (res.equalsIgnoreCase("n") || res.equalsIgnoreCase("no")) {
                return true;
            } else {
                System.out.println("Please consider only the choices Y(Yes) or N(No)");
            }
        }
        System.out.println("Booking canceled with success!");
        return true;
    }

    /**
     * The headline to be presented in the startup of the UC.
     *
     * @return the headline as a String
     */
    @Override
    public String headline() {
        return "Booking Cancelation";
    }

}
