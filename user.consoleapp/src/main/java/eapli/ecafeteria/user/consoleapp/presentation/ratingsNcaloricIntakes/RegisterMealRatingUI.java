package eapli.ecafeteria.user.consoleapp.presentation.ratingsNcaloricIntakes;

import eapli.ecafeteria.application.ratings.RegisterMealRatingController;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.user.consoleapp.presentation.ShowBalance;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1150585 / 1150801
 */
public class RegisterMealRatingUI extends AbstractUI implements ShowBalance {

    private final RegisterMealRatingController theController = new RegisterMealRatingController();

    @Override
    protected boolean doShow() {

        showBalance();

        final CafeteriaUser user;

        user = theController.getUser();

        if (user != null) {
            theController.setUser(user);
        } else {
            System.out.println("User is not registered as a Cafeteria User!!!");
        }

        MealType typeOfMeal = null;
        String typeMeal = "not specified";
        // Displays a message on the console and provides a field to fill with a date.
        final Date mealDate = Console.readDate("Meal Date (DD/MM/YYYY):");

        while (!typeMeal.equalsIgnoreCase("lunch") || !typeMeal.equalsIgnoreCase("dinner")) {
            // Displays a message on the console and provides a field to fill with a string.
            typeMeal = Console.readLine("Type of Meal(Lunch or Dinner):");

            if (typeMeal.equalsIgnoreCase("lunch")) {
                typeOfMeal = MealType.LUNCH;
                break;
            } else if (typeMeal.equalsIgnoreCase("dinner")) {
                typeOfMeal = MealType.DINNER;
                break;
            }

        }
        // Displays a message on the console and provides a field to fill with a number.
        final int rating = Console.readInteger("Rating (1-5):");

        if (this.theController.checkBooking(mealDate, typeOfMeal)) {

            // Displays a message on the console and provides a field to fill with a number for confirmation purposes.
            final int confirmation = Console.readInteger("To save this information press 1");

            if (confirmation == 1) {

                try {
                    this.theController.registerNewRatingWithComment(mealDate, typeOfMeal, rating);
                } catch (final DataConcurrencyException | DataIntegrityViolationException e) {
                    System.out.println("Only one rating per meal is allowed");
                }

                // Displays a message on the console and provides a field to fill with a number for confirmation purposes.
                final int comments = Console.readInteger("To insert comment press 1. To exit press 0");

                if (comments == 1) {

                    // Displays a message on the console and provides a field to fill with string.
                    final String com = Console.readLine("Insert a comment");

                    // Displays a message on the console and provides a field to fill with a number for confirmation purposes.
                    final int commentConfirmation = Console.readInteger("To save press 1");

                    if (commentConfirmation == 1) {
                        try {
                            theController.createComment(com);
                        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                            System.out.println("There is already a comment and a rating this meal");
                        }
                    }
                    System.out.println("Thank you for your rating and comment!");
                } else {
                    try {
                        theController.registerNewRatingWithoutComment(mealDate, typeOfMeal, rating);
                    } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                        System.out.println("There is already a comment and a rating this meal");
                    }
                        System.out.println("Thank you!");
                }
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Register Meal Rating";
    }

}
