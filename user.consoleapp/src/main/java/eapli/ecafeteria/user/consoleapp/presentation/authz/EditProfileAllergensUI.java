/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.authz;

import eapli.ecafeteria.application.authz.EditProfileAllergensController;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.List;

/**
 *
 * @author Nuno
 */
public class EditProfileAllergensUI extends AbstractUI {

    private final EditProfileAllergensController theController = new EditProfileAllergensController();

    @Override
    protected boolean doShow() {

        do {
            theController.loggedUser();
            List<Allergen> allergenProfileList = theController.profileAllergenList();
            if (allergenProfileList.isEmpty()) {
                System.out.println("Your profile doesn't have any assiciated allergen.");
            } else {
                System.out.println("Your profile's current allergen List:");
                for (Allergen profileAlergen : allergenProfileList) {
                    System.out.println(profileAlergen.toString());
                }
            }

            System.out.println("\nDo you wish to:\n1. Add an allergen to the list.\n2. Remove an allergen to the list.\n0. Exit");
            int option = Console.readOption(1, 2, 0);
            if (option == 0) {
                System.out.println("\nEdit Profile's Allergens finished.");
                break;
            }

            System.out.println("");
            boolean success = false;
            if (option == 1) {
                SelectWidget<Allergen> allergenSelector = new SelectWidget<>("Select the allergen you wish to add:", theController.allAllergenList());
                allergenSelector.show();

                Allergen selectedAllergen = allergenSelector.selectedElement();

                System.out.println("");
                if (selectedAllergen != null) {
                    success = theController.addAllergen(selectedAllergen);
                    if (!success) {
                        System.out.println("Your profile's allergen list already contains that allergen.\n");
                    }
                } else {
                    System.out.println("Operation canceled.\n");
                }

            } else {
                SelectWidget<Allergen> allergenSelector = new SelectWidget<>("Allergen List:", theController.profileAllergenList());
                allergenSelector.show();

                Allergen selectedAllergen = allergenSelector.selectedElement();

                System.out.println("");
                if (selectedAllergen != null) {
                    success = theController.removeAllergen(selectedAllergen);
                    if (!success) {
                        System.out.println("Your profile's allergen list doesn't contain the chosen allergen.\n");
                    }
                } else {
                    System.out.println("Operation canceled.\n");
                }

            }

            if (success) {
                try {
                    if (theController.save() != null) {
                        System.out.println("Your profile's allergen list was successefully updated.\n");
                    }
                } catch (DataConcurrencyException ex) {
                    System.out.println("It wasn't possible to make any alteration, since this profile has changed since it was read.\n");
                } catch (DataIntegrityViolationException ex) {
                    System.out.println("The inserted data is not valid.\n");
                }
            }
        } while (true);
        return false;
    }

    @Override

    public String headline() {
        return "Edit Allgerns.";
    }

}
