package eapli.ecafeteria.user.consoleapp.presentation.bookingMeal;

import eapli.ecafeteria.application.booking.BookingMealController;
import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.user.consoleapp.presentation.ShowBalance;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.DateTime;
import eapli.util.io.Console;
import java.util.Calendar;
import java.util.List;

public class BookingMealUI extends AbstractUI  implements ShowBalance {

    private final BookingMealController controller = new BookingMealController();

    @Override
    protected boolean doShow() {
        showBalance();

        if (controller.getCurrentUser() == null) {
            System.out.println("\nERROR: Current user is not a cafeteria user.\n");
            return false;
        }

        Calendar cal;

        do {
            String dateMeal = Console.readLine("\nIntroduce the meal date with the format 'dd-mm-yyyy' : ");
            cal = DateTime.parseDate(dateMeal);

            if (cal == null) {
                System.out.println("\nERROR: Date introduced in invalid format.");
            }

        } while (cal == null);

        MealType type = null;

        do {
            String typeMeal = Console.readLine("\nIntroduce the type of meal (lunch or dinner): ");

            if (typeMeal.equalsIgnoreCase("lunch")) {
                type = MealType.LUNCH;
            } else if (typeMeal.equalsIgnoreCase("dinner")) {
                type = MealType.DINNER;
            } else {
                System.out.println("\nERROR: Type of meal introduced invalid.");
            }
        } while (type == null);

        List<Meal> meals = (List<Meal>) controller.getMeals(cal, type);

        if (meals.isEmpty()) {
            System.out.println("\nERROR: No meals available of the specified type on the date introduced.\n");
            return false;
        }

        boolean canAffordMeal;  // true if user has enough balance to book the selected meal. false otherwise.
        Meal selectedMeal;
        String answer;

        do {
            answer = "y";
            
            SelectWidget<Meal> mealsWidget = new SelectWidget<>("\nSelect meal: ", meals);
            mealsWidget.show();

            // Checks if user selected the option to exit
            if (mealsWidget.selectedOption() == 0) {
                return false;
            }

            selectedMeal = mealsWidget.selectedElement();

            canAffordMeal = controller.canUserAffordMeal(selectedMeal);

            // Checks if user can afford the selected meal
            if (!canAffordMeal) {
                System.out.println("\nERROR: Not enough balance in card account to book the selected meal.");
            }
            
            else {
                // Checks if the meal contains user's allergens
                if(controller.doesMealHaveUserAllergens(selectedMeal)) {
                    do {
                        answer = Console.readLine("\nThe selected meal contains allergens present in your profile. Are you sure you want to book it (Y or N)?\n");

                        if(!answer.equalsIgnoreCase("n") && !answer.equalsIgnoreCase("y"))
                            System.out.println("\nERROR: Invalid answer.");
                            
                    } while(!answer.equalsIgnoreCase("n") && !answer.equalsIgnoreCase("y"));
                }
            }

        } while (!canAffordMeal || answer.equals("n"));
        
        Booking booking;

        try {
            booking = controller.bookMeal(selectedMeal);

            // Checks if booking was saved and the user's balance was updated succesfully
            if (booking == null) {
                System.out.println("\nERROR: There was a problem booking the meal.\n");
            } else {
                System.out.println("\nMeal booked successfully!\n");
            }

        } catch (DataConcurrencyException ex) {
            System.out.println("\nERROR: There was a problem booking the meal.\n");
        } catch (DataIntegrityViolationException ex) {
            System.out.println("\nERROR: You already have a booking for the selected meal.\n");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Book Meal";
    }
}
