/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.bookingMeal;

import eapli.ecafeteria.application.booking.CheckBookingNextDaysController;
import eapli.ecafeteria.application.kitchen.QuantityMealsCookedController;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;

/**
 *
 * @author Goncalo
 */
public class CheckBookingNextDaysUI extends AbstractUI{
    
    private final CheckBookingNextDaysController theController = new CheckBookingNextDaysController();

    @Override
    protected boolean doShow() {

        boolean validValue = false;
        int nDays;
        do {
            //Reports the number of days
            nDays = Console.readInteger("\nInsert the number of days that will size the interval. Type '0' to exit.");
            try {
                theController.insertNumberOfDays(nDays);
                if (nDays == 0) {
                    System.out.println("\nOperation canceled");
                    return false;
                } else {
                    validValue = true;
                }
                        
            } catch (final IllegalArgumentException ex) {
                System.out.println("\nThe inserted value is not valid.");
            }
        } while (validValue != true);
        
        
        int size = theController.getBookingNextDays().size();
        
        System.out.println("Meals for the next "+nDays+" days:");
        
        for (int i = 0; i < size; i++) {
            System.out.println(i+" - "+theController.getBookingNextDays().get(i)); //Lists the bookings 
        }
        
        int exit;
        exit = Console.readInteger("\nType '0' to exit.");
        
        return true;
    }

    @Override
    public String headline() {
        return "Check Booking for the next days";
    }
    
}
