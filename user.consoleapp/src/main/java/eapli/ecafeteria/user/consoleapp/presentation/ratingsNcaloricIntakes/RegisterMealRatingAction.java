/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.ratingsNcaloricIntakes;

import eapli.framework.actions.Action;

/**
 *
 * @author Daniel Fernandes
 */
public class RegisterMealRatingAction implements Action {

    @Override
    public boolean execute() {
        return new RegisterMealRatingUI().show();
    }

}
