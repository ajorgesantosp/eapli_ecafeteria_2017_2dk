/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.bookingMeal;

import eapli.ecafeteria.domain.booking.Booking;
import eapli.framework.visitor.Visitor;
import java.text.SimpleDateFormat;

/**
 *
 * @author César Seabra
 */
public class BookingPrinter implements Visitor<Booking> {

    @Override
    public void visit(Booking visitee) {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        String date = format1.format(visitee.meal().date().getTime());
        System.out.printf("Data: %-11s Dish: %-20s Dish Type: %-15s Meal Type: %-10s\n", date, visitee.meal().dish().id().toString(), visitee.meal().dish().dishType().toString(),visitee.meal().mealType());
    }

    @Override
    public void beforeVisiting(Booking visitee) {
        //nothing to do
    }

    @Override
    public void afterVisiting(Booking visitee) {
        //nothing to do
    }

}
