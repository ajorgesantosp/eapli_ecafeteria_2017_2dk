package eapli.ecafeteria.user.consoleapp.presentation.bookingMeal;

import eapli.framework.actions.Action;

public class BookingMealAction implements Action {

    @Override
    public boolean execute() {
        return new BookingMealUI().show();
    }

}

