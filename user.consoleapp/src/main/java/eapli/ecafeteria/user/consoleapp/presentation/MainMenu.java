/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation;

import eapli.ecafeteria.user.consoleapp.presentation.ratingsNcaloricIntakes.RegisterMealRatingAction;
import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.cafeteria.consoleapp.presentation.MyUserMenu;
import eapli.ecafeteria.application.CafeteriaUserBaseController;
import eapli.ecafeteria.user.consoleapp.presentation.authz.EditProfileAllergensAction;
import eapli.ecafeteria.user.consoleapp.presentation.authz.EditProfileCaloriesPerMealAction;
import eapli.ecafeteria.user.consoleapp.presentation.authz.EditProfileCaloriesPerWeekAction;
import eapli.ecafeteria.user.consoleapp.presentation.authz.EditProfileSaltPerWeekAction;
import eapli.ecafeteria.user.consoleapp.presentation.bookingMeal.BookingMealAction;
import eapli.ecafeteria.user.consoleapp.presentation.bookingMeal.BookingMealCancelAction;
import eapli.ecafeteria.user.consoleapp.presentation.bookingMeal.CheckBookingNextDaysAction;
import eapli.framework.actions.ReturnAction;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.ShowMessageAction;
import eapli.framework.presentation.console.ShowVerticalSubMenuAction;
import eapli.framework.presentation.console.SubMenu;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.presentation.console.VerticalSeparator;

/**
 * @author Paulo Gandra Sousa
 */
class MainMenu extends CafeteriaUserBaseUI  {

    private static final int EXIT_OPTION = 0;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int BOOKINGS_OPTION = 2;
    private static final int ACCOUNT_OPTION = 3;
    private static final int RATINGS_CALORIES_OPTION = 4;
    private static final int EDIT_PROFILE_OPTION = 5;

    // BOOKINGS MENU
    private static final int LIST_MENUS_OPTION = 1;
    private static final int BOOK_A_MEAL_OPTION = 2;
    private static final int CHECK_BOOKING_NEXT_DAYS =3;
    private static final int CANCEL_BOOKING = 4;
    

    // ACCOUNT MENU
    private static final int LIST_MOVEMENTS_OPTION = 1;

    //RATING/CHECK CALORIC INTAKES
    private static final int CHECK_CALORIES_INTAKE_OPTION = 1;
    private static final int REGISTER_MEAL_RATING = 2;
    private static final int CHECK_RATINGS_AND_COMMENTS = 3;

    //EDIT PROFILE
    private static final int CHANGE_CALORIES_OPTION = 1;
    private static final int CHANGE_SALT_OPTION = 2;
    private static final int CHANGE_MEAL_CALORIES_OPTION = 3;
    private static final int CHANGE_ALLERGENS_OPTION = 4;

    @Override
    public boolean show() {
        
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer = new VerticalMenuRenderer(menu);
        return renderer.show();
    }

    private Menu buildMainMenu() {
       drawFormTitle();
        final Menu mainMenu = new Menu();
     
        final Menu myUserMenu = new MyUserMenu();
        mainMenu.add(new SubMenu(MY_USER_OPTION, myUserMenu, new ShowVerticalSubMenuAction(myUserMenu)));

        mainMenu.add(VerticalSeparator.separator());

        final Menu bookingsMenu = buildBookingsMenu();
        mainMenu.add(new SubMenu(BOOKINGS_OPTION, bookingsMenu, new ShowVerticalSubMenuAction(bookingsMenu)));

        mainMenu.add(VerticalSeparator.separator());

        final Menu accountMenu = buildAccountMenu();
        mainMenu.add(new SubMenu(ACCOUNT_OPTION, accountMenu, new ShowVerticalSubMenuAction(accountMenu)));

        mainMenu.add(VerticalSeparator.separator());

        final Menu ratingMenu = buildRatingAndCaloriesMenu();
        mainMenu.add(new SubMenu(RATINGS_CALORIES_OPTION, ratingMenu, new ShowVerticalSubMenuAction(ratingMenu)));

        // TODO add menu options
        mainMenu.add(VerticalSeparator.separator());

        final Menu editProfileMenu = buildEditProfileMenu();
        mainMenu.add(new SubMenu(EDIT_PROFILE_OPTION, editProfileMenu, new ShowVerticalSubMenuAction(editProfileMenu)));

        mainMenu.add(VerticalSeparator.separator());

        mainMenu.add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));

        return mainMenu;
    }

    private Menu buildEditProfileMenu() {
        final Menu menu = new Menu("Edit Profile");
        menu.add(new MenuItem(CHANGE_CALORIES_OPTION, "Change calorie intake per week.", new EditProfileCaloriesPerWeekAction()));
        menu.add(new MenuItem(CHANGE_SALT_OPTION, "Change salt intake per week. ", new EditProfileSaltPerWeekAction()));
        menu.add(new MenuItem(CHANGE_MEAL_CALORIES_OPTION, "Change calorie intake per meal. ", new EditProfileCaloriesPerMealAction()));
        menu.add(new MenuItem(CHANGE_ALLERGENS_OPTION, "Change allergens.", new EditProfileAllergensAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }

    private Menu buildAccountMenu() {
        final Menu menu = new Menu("Account");
        menu.add(new MenuItem(LIST_MOVEMENTS_OPTION, "List movements", new ShowMessageAction("Not implemented yet")));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }

    private Menu buildBookingsMenu() {
        final Menu menu = new Menu("Bookings");
        menu.add(new MenuItem(LIST_MENUS_OPTION, "List menus", new ShowMessageAction("Not implemented yet")));
        menu.add(new MenuItem(BOOK_A_MEAL_OPTION, "Book a meal", new BookingMealAction()));
        menu.add(new MenuItem(CHECK_BOOKING_NEXT_DAYS, "Check Bookings for the next days", new CheckBookingNextDaysAction()));
        menu.add(new MenuItem(CANCEL_BOOKING, "Cancel Booking", new BookingMealCancelAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }

    private Menu buildRatingAndCaloriesMenu() {
        final Menu menu = new Menu("Ratings/Check Caloric Intakes");
        menu.add(new MenuItem(CHECK_CALORIES_INTAKE_OPTION, "Check calories intake option", new ShowMessageAction("Not implemented yet")));
        menu.add(new MenuItem(REGISTER_MEAL_RATING, "Register meal rating", new RegisterMealRatingAction()));
        menu.add(new MenuItem(CHECK_RATINGS_AND_COMMENTS, "Check Ratings and comments ", new ShowMessageAction("Not implemented yet")));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }

   

   
  
    
}
