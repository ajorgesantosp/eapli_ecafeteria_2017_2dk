/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.PublishMenuController;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Leandro Santos
 */
public class PublishMenuUI extends AbstractUI {

    final PublishMenuController theController = new PublishMenuController();
    
    @Override
    protected boolean doShow() {
        System.out.println(headline() + "\n");
        List<Menu> menuList = (List<Menu>) theController.listMenusInProgress();
        if (menuList.isEmpty()) {
            System.out.println("There are no menus in progress.");
            return false;
        }else {
                SelectWidget<Menu> menuSelector = new SelectWidget("\nAvailable Menus:", menuList);
                menuSelector.show();
                theController.chooseMenu(menuSelector.selectedElement());
                
        
            try {
                if (theController.publishMenu(menuSelector.selectedElement()) == null) {
                    System.out.println("The menu you are trying to publish was already published.");
                }else{
                   System.out.println("Menu successfully published!"); 
                }
            } catch (DataIntegrityViolationException ex) {
                Logger.getLogger(PublishMenuUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DataConcurrencyException ex) {
                Logger.getLogger(PublishMenuUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException ex) {
                Logger.getLogger(PublishMenuUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    
    @Override
    public String headline() {
        return "Publish Menu";
    }

}
