/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.RegisterBatchController;
import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.domain.kitchen.Material;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;

/**
 *
 * @author diogo
 */
public class RegisterBatchUI extends AbstractUI {

    private final RegisterBatchController theController = new RegisterBatchController();

    @Override
    protected boolean doShow() {
        
        Iterable<Material> allMaterials = theController.getAvaliableMaterials();
        
        // Checks if there are any material available
        if (!allMaterials.iterator().hasNext()) {
            System.out.println("\nERROR: There are no materials available.\n");
            return false;
        }
        
        final SelectWidget<Material> selector = new SelectWidget<>("Materials", allMaterials);
        selector.show();
        
        // Checks if user selected the option to exit
        if (selector.selectedOption() == 0) {
            return false;
        }
            
        final Material material = selector.selectedElement();
        
        final int quantity = Console.readInteger("\nIntroduce the batch's quantity: ");
        
        final String description = Console.readLine("\nIntroduce the batch's description: ");
        
        String acronym;
        Batch batch = null;
        
        do{
            acronym = Console.readLine("\nIntroduce the batch's acronym: ");
        
            try {
                batch = theController.registerBatch(description, acronym, material, quantity);
                
                // Checks if batch was created successfully
                if (batch == null) {
                    System.out.println("\nERROR: There was a problem registering the batch.\n");
                    return false;
                }
                
                System.out.println("\nBatch created successfully!\n");

            } catch (DataConcurrencyException ex) {
                System.out.println("\nERROR: There was a problem registering the batch.\n");
                return false;
            } catch (DataIntegrityViolationException ex) {
                System.out.println("\nERROR: The acronym introduced is already in use.");
                acronym = null;            
            }
        
        } while(acronym == null);
        
        Iterable<Meal> allMeals = theController.getAvaliableMeals();
        
        // Checks if there are any meals available
        if (!allMeals.iterator().hasNext()) {
            System.out.println("\nERROR: There are no meals available.\n");
            return false;
        }
        
        final SelectWidget<Meal> mealsWidget = new SelectWidget<>("Select meal to add batch to", allMeals);

        String answer;
        
        do {
            answer = "y";
            
            mealsWidget.show();
        
            // Checks if user selected the option to exit
            if (mealsWidget.selectedOption() == 0) {
                return false;
            }

            Meal selectedMeal = mealsWidget.selectedElement();
                    
            try {
                selectedMeal = theController.addBatchToMeal(selectedMeal, batch);

                if (selectedMeal == null) {
                    System.out.println("\nERROR: There was a problem adding the batch to the selected meal.\n");
                }

                else {
                    System.out.println("\nBatch added successfully!");

                    do {
                        answer = Console.readLine("\nDo you want to add the batch to any more meals (Y or N)?");

                        if(!answer.equalsIgnoreCase("n") && !answer.equalsIgnoreCase("y"))
                            System.out.println("\nERROR: Invalid answer.");

                    } while(!answer.equalsIgnoreCase("n") && !answer.equalsIgnoreCase("y"));
                }

            } catch(DataConcurrencyException ex) {
                System.out.println("\nERROR: There was a problem adding the batch to the selected meal.\n");
            } catch(DataIntegrityViolationException ex) {
                System.out.println("\nERROR: You already added the batch to the selected meal.\n");
            }
        } while(answer.equalsIgnoreCase("y"));
        
        return false;
    }

    @Override
    public String headline() {
        return "Register Batch";
    }
}
