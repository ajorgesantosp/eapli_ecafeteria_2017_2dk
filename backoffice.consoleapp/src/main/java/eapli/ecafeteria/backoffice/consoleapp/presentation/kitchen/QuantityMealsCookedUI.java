/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.QuantityMealsCookedController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.List;

/**
 *
 * @author Goncalo (1150924)
 */
public class QuantityMealsCookedUI extends AbstractUI {

    private final QuantityMealsCookedController theController = new QuantityMealsCookedController();

    @Override
    protected boolean doShow() {

        //Asks for the day
        boolean valido = false;
        while (valido == false) {
            final String mealDate = Console.readLine("\nInsert the Meal's Date with the format 'dd-mm-yyyy'. To exit type '0'.");
            if (mealDate.equals("0")) {
                System.out.println("\nOperation canceled.");
                return false;
            }
            if (theController.insertDate(mealDate)) {
                valido = true;
            } else {
                System.out.println("\nInvalid date.");
            }
        }

        //Asks for the type of meal
        SelectWidget<MealType> mealTypeSelector = new SelectWidget<>("\nSelect the meal type.", theController.allMealTypes());
        mealTypeSelector.show();
        MealType selectedMealType = mealTypeSelector.selectedElement();
        if (selectedMealType == null) {
            System.out.println("\nThe operation was canceled.");
            return false;
        }
        theController.insertMealType(selectedMealType);

        List<Meal> plannedMealsList = (List<Meal>) theController.mealsByDateAndType();
        //If the list is empty, the process cant continue
        if (plannedMealsList.isEmpty()) {
            System.out.println("There are no planned meals!");
            return false;
        }

        //Chooses the meal using the index associated with the meal, if the user choses 0, it exits the selection
        SelectWidget<Meal> mealSelector = new SelectWidget<>("Select the which meal you want to report the number of dishes made", plannedMealsList);
        mealSelector.show();
        Meal selectedMeal = mealSelector.selectedElement();
        if (selectedMeal == null) {
            System.out.println("\nThe operation was canceled.");
            return false;
        }

        boolean validValue = false;
        int numberDishes;
        do {
            //Reports the doses
            numberDishes = Console.readInteger("\nInsert the number of cooked dishes for the selected meal.");
            try {
                theController.insertQuantity(numberDishes, selectedMeal);
                if (numberDishes == 0) {
                    System.out.println("\nOperation canceled");
                    return false;
                }else{
                    validValue=true;
                }
            } catch (final IllegalArgumentException ex) {
                System.out.println("\nThe inserted value is not valid.");
            }
        } while (validValue != true);

        //By confirming, it saves the information gathered in the database
        System.out.println("\nDo you confirm the entered information?\n1. Confirm\n0. Cancel");
        final int confirmation = Console.readOption(1, 1, 0);
        if (confirmation == 1) {
            try {
                if (theController.save(selectedMeal) == null) {
                    System.out.println("\nAn unexpected error ocurred the data wasn't successfully saved.");
                    return false;
                }
            } catch (DataConcurrencyException ex) {
                System.out.println("\nIt wasn't possible to save the inserted data since the meal was updated since the last time it was read.");
                return false;
            } catch (DataIntegrityViolationException ex) {
                System.out.println("\nThe inserted data is invalid.");
                return false;
            }
        } else {
            System.out.println("\nOperation canceled.");
            return false;
        }

        System.out.println("\nMeal successfully updated!");
        return false;
    }

    @Override
    public String headline() {
        return "Number Dishes Made";
    }

}
