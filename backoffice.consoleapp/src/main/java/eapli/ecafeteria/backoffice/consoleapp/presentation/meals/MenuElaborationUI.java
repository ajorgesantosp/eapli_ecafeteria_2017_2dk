/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.MenuElaborationController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.DateTime;
import eapli.util.io.Console;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Leandro Santos
 */
public class MenuElaborationUI extends AbstractUI {

    final MenuElaborationController theController = new MenuElaborationController();

    @Override
    protected boolean doShow() {

        System.out.println(headline());

        System.out.println("\nDo you wish to:\n1. Edit an existing menu.\n2. Start the creation of a new menu.");
        int op = Console.readOption(1, 2, 0);

        if (op == 1) {
            List<Menu> menuList = (List<Menu>) theController.menus();
            if (menuList.isEmpty()) {
                System.out.println("\nThere are no menus in progress.");
                return false;
            } else {
                SelectWidget<Menu> menuSelector = new SelectWidget("\nAvailable Menus:", menuList);
                menuSelector.show();
                Menu chosenMenu = menuSelector.selectedElement();
                if (chosenMenu == null) {
                    System.out.println("\nOperation canceled.");
                    return false;
                }
                theController.chooseMenu(chosenMenu);
            }
        } else {

            Calendar startDate = askDate("\nIntroduce the starting date of the menu with the format 'dd-mm-yyyy'. To exit type '0'.");
            if (startDate == null) {
                System.out.println("\nOperation canceled.");
                return false;
            }

            Calendar endDate = askDate("\nIntroduce the ending date of the menu with the format 'dd-mm-yyyy'. To exit type '0'.");
            if (endDate == null) {
                System.out.println("\nOperation canceled.");
                return false;
            }

//            if (endDate.before(startDate)) {
//                System.out.println("\nThe ending date of the menu can't be before the starting date.");
//                return false;
//            }
//            if (startDate.before(DateTime.now())) {
//                System.out.println("\nThe starting date can't be prior to today's date.");
//                return false;
//            }
            Menu newMenu = theController.elaborateMenu(startDate, endDate);
            if (newMenu == null) {
                System.out.println("\nThere is an already created menu for the introduced dates.");
                return false;
            }
            try {
                if (theController.saveMenu() == null) {
                    System.out.println("\nAn unexpected error ocurred saving the changes made to the menu.");
                    return false;
                }
                System.out.println("Menu successfully updated!");
            } catch (final DataIntegrityViolationException | DataConcurrencyException ex) {
                System.out.println("\nThe created menu already exits.");
                return false;
            }
        }

        while (true) {
            System.out.println("Do you wish to:\n1. Add a meal \n2. Remove a meal\n0. Finish");
            int newOp = Console.readOption(1, 2, 0);
            switch (newOp) {
                case 1:
                    List<Meal> mealList = theController.availableMeals();
                    if (mealList.isEmpty()) {
                        System.out.println("\nThere aren't any published meals for the days of the menu.");
                        break;
                    }
                    SelectWidget<Meal> mealSelector = new SelectWidget("\nMeal to add:", mealList);
                    mealSelector.show();
                    if (theController.addMeal(mealSelector.selectedElement())) {
                        try {
                            if (theController.saveMenu() == null) {
                                System.out.println("An unexpected error ocurred saving the changes made to the menu.");
                                return false;
                            }
                            System.out.println("Menu successfully updated!");
                        } catch (final DataIntegrityViolationException | DataConcurrencyException ex) {
                            System.out.println("\nERROR: The menu you are editing was changed since it was last read.");
                            return false;
                        }
                    } else {
                        System.out.println("\nThe meal you are trying to add is already in the menu.");
                    }
                    break;

                case 2:
                    SelectWidget<Meal> otherSelector = new SelectWidget("\nMeal to remove:", theController.menuMealList());
                    if (otherSelector.selectedElement() == null) {
                        return false;
                    }
                    if (theController.removeMeal(otherSelector.selectedElement())) {
                        try {
                            if (theController.saveMenu() == null) {
                                System.out.println("An unexpected error ocurred saving the changes made to the menu.");
                                return false;
                            }
                            System.out.println("Menu successfully updated!");
                        } catch (final DataIntegrityViolationException | DataConcurrencyException ex) {
                            System.out.println("\nERROR: The menu you are editing was changed since it was last read.");
                            return false;
                        }
                    }
                    break;

                default:
                    return false;
            }

        }
    }

    @Override
    public String headline() {
        return "Elaborate a Menu.";
    }

    private Calendar askDate(String dateInfo) {
        do {
            String dateString = Console.readLine(dateInfo);
            if ("0".equals(dateString)) {
                return null;
            }
            Calendar date = theController.parseDate(dateString);
            if (date != null) {
                return date;
            }
            System.out.println("\nInvalid date.");
        } while (true);
    }

}
