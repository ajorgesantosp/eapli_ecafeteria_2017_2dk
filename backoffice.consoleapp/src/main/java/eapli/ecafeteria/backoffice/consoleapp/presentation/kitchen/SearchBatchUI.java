/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.SearchBatchController;
import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;

/**
 *
 * @author diogo
 */
public class SearchBatchUI extends AbstractUI {

    private final SearchBatchController theController = new SearchBatchController();

    /**
     * show messages to user interacte with the system and show meals by batch.
     *
     * @return meals by batch or false
     */
    protected boolean doShow() {

        String answer;
        
        do {
            String acronym = Console.readLine("\nIntroduce the batch's acronym: ");

            Batch batch = theController.findByAcronym(acronym);

            // Checks if there is any batch with the introduced acronym
            if (batch == null) {
                System.out.println("\nERROR: There is no batch with the introduced acronym.\n");
                return false;
            }

            Iterable<Meal> meals = theController.getMealsByBatch(batch);
            
            int cont = 1;
            
            // Checks if there are any meals using the introduced batch
            if(!meals.iterator().hasNext())
                System.out.println("\nERROR: There are no meals using the introduced batch.");

            else {
                System.out.println("\nMeals using the introduced batch:");
                
                for (Meal meal : meals) {
                    System.out.printf("%d. %s\n", cont, meal.toString());
                    cont++;
                }
            }

            do {
                answer = Console.readLine("\nDo you want to search meals by another batch (Y or N)?");

                if (!answer.equalsIgnoreCase("n") && !answer.equalsIgnoreCase("y")) {
                    System.out.println("\nERROR: Invalid answer.");
                }

            } while (!answer.equalsIgnoreCase("n") && !answer.equalsIgnoreCase("y"));
        } while(answer.equalsIgnoreCase("y"));
            
        return false;
    }

    /**
     * Headline
     *
     * @return Search Batch Headline
     */
    @Override
    public String headline() {
        return "Search Meal by Batch";
    }
}
