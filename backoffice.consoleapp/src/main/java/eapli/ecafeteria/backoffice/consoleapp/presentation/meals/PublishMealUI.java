/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.PublishMealController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import java.util.Scanner;

/**
 *
 * Created by 1150450 and 1150444 on 02/05/2017.
 */
public class PublishMealUI extends AbstractUI {

    private final PublishMealController theController = new PublishMealController();
    private final Scanner in = new Scanner(System.in);

    private final String MEALTYPE_HEADER = "\nSelect the meal type.";
    private final String DISH_HEADER = "\nSelect the dish.";
    private final String EXIT_MESSAGE = "\nThe operation was canceled.";

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        System.out.println(headline());

        SelectWidget<MealType> mealTypeSelector = new SelectWidget<>(MEALTYPE_HEADER, theController.allMealTypes());
        mealTypeSelector.show();
        MealType selectedMealType = mealTypeSelector.selectedElement();
        if (selectedMealType == null) {
            System.out.println(EXIT_MESSAGE);
            return false;
        }

        SelectWidget<Dish> dishSelector = new SelectWidget<>(DISH_HEADER, theController.allActiveDishes());
        dishSelector.show();
        Dish selectedDish = dishSelector.selectedElement();
        if (selectedDish == null) {
            System.out.println(EXIT_MESSAGE);
            return false;
        }

        boolean accepetedDate = false;
        System.out.println("\nIntroduce the date of the meal with the format 'dd-mm-yyyy'. To exit type '0'.");
        do {
            String date = in.next();
            if (date.equals("0")) {
                System.out.println(EXIT_MESSAGE);
                return false;
            }

            try {
                Meal newMeal = theController.buildMeal(selectedMealType, selectedDish, date);
                if (newMeal == null) {
                    System.out.println("\nAn error ocurred parsing the date.\n"
                            + "Please introduce it again with the format 'dd-mm-yyyy'. To exit type '0'.");
                } else {
                    accepetedDate = true;
                    if (theController.publishMeal(newMeal) == null) {
                        System.out.println("\nThe meal you are publishing already exists.");
                        return false;
                    }
                }
            } catch (final DataIntegrityViolationException | DataConcurrencyException ex) {
                System.out.println("\nThe introduced data is invalid.");
                return false;
            }
        } while (!accepetedDate);

        System.out.println("\nMeal successfully published!");

        return false;
    }

    @Override
    public String headline() {
        return "Publish a Meal.";
    }

}
