/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.ListMealTypeController;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

/**
 *
 * Created by 1150450 and 1150444 on 04/05/2017.
 */
public class ListMealTypeUI extends AbstractListUI<MealType> {

    private final ListMealTypeController theController = new ListMealTypeController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected Iterable<MealType> listOfElements() {
        return this.theController.allMealTypes();
    }

    @Override
    protected Visitor<MealType> elementPrinter() {
        return new MealTypePrinter();
    }

    @Override
    protected String elementName() {
        return "Meal Type";
    }

}
