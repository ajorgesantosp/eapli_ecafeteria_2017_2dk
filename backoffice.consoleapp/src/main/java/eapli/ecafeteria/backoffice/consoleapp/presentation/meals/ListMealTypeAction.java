/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.framework.actions.Action;

/**
 *
 * Created by 1150450 and 1150444 on 02/05/2017.
 */
public class ListMealTypeAction implements Action {

    @Override
    public boolean execute() {
        return new ListMealTypeUI().show();
    }

}
