/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.visitor.Visitor;

/**
 *
 * Created by 1150450 and 1150444 on 04/05/2017.
 */
public class MealTypePrinter implements Visitor<MealType>{

    @Override
    public void visit(MealType visitee) {
        System.out.println(visitee.toString());
    }
    
}
