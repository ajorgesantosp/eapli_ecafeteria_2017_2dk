/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.util.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author fabio
 */
public class QRCodeTest {

    public QRCodeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    /**
     * Test of readFromFile method, of class QRCode.
     * @throws java.io.FileNotFoundException
     */
    
    @Test
    public void testReadFromFile() throws FileNotFoundException, IOException {
        String content = "12";
        boolean res = QRCode.createQRCode(content,content);
        System.out.println("readFromFile");
        String filePath = "../Resources/qrcodes/" + content + ".png";
        String result = QRCode.readFromFile(filePath);

        File f = new File(filePath);
        f.delete();

        assertEquals("12", result);
    }
    

    /**
     * Test of createQRCode method, of class QRCode.
     *
     * @throws java.lang.Exception
     */
    
    @Test
    public void testCreateQRCode() throws Exception {
        System.out.println("createQRCode");
        boolean result;
        String idBooking = "2345DF";
        result = QRCode.createQRCode(idBooking, idBooking);
        if (result) {
            File file = new File("../Resources/qrcodes/2345DF.png");
            file.delete();
        }
        assertTrue(result);
    }
    

}
