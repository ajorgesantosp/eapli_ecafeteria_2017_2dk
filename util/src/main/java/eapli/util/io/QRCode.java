package eapli.util.io;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;
import javax.imageio.ImageIO;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 *
 * @author fabio e cesar
 */
public class QRCode {

    /**
     * This method is used to return the content read from a QRCode on the input
     * file.
     *
     * @param filePath Path to the image file containing the QRCode
     * @return the message present in the QRCode image. In this concrete case it
     * will be the reservation code.
     * @throws java.io.FileNotFoundException
     */
    public static String readFromFile(String filePath) throws FileNotFoundException, IOException {
        Result result = null;
        BinaryBitmap binaryBitmap;
        FileInputStream out = new FileInputStream(filePath);
        try {

            binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(out))));
            result = new MultiFormatReader().decode(binaryBitmap);
        } catch (NotFoundException | IOException ex) {
        }
        out.close();
        return result.getText();

    }

    /**
     * Creates a new QRCode image in the specified folder /Resources/qrcodes.
     * The image will contain the ID of the booking.
     *
     * @param content
     * @param fileName
     * @return
     */
    public static boolean createQRCode(String content, String fileName) {
        String filePath = "../Resources/qrcodes/" + fileName + ".png";
        int size = 250;
        String fileType = "png";
        File myFile = new File(filePath);
        try {

            Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");

            // Now with zxing version 3.2.1 you could change border size (white border size to just 1)
            hintMap.put(EncodeHintType.MARGIN, 1);
            /* default = 4 */
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix byteMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, size,
                    size, hintMap);
            int CrunchifyWidth = byteMatrix.getWidth();
            BufferedImage image = new BufferedImage(CrunchifyWidth, CrunchifyWidth,
                    BufferedImage.TYPE_INT_RGB);
            image.createGraphics();

            Graphics2D graphics = (Graphics2D) image.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);
            graphics.setColor(Color.BLACK);

            for (int i = 0; i < CrunchifyWidth; i++) {
                for (int j = 0; j < CrunchifyWidth; j++) {
                    if (byteMatrix.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }
            ImageIO.write(image, fileType, myFile);

            return true;
        } catch (WriterException | IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
