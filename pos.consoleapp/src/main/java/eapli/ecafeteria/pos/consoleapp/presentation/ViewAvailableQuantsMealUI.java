/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.ecafeteria.application.cashier.ViewAvailableQuantsMealController;
import eapli.ecafeteria.domain.delivery.CashierState;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.presentation.console.AbstractUI;
import java.util.Map;

/**
 *
 * @author 1150450 and 1150585
 */
public class ViewAvailableQuantsMealUI extends AbstractUI {

    private final ViewAvailableQuantsMealController theController = new ViewAvailableQuantsMealController();

    @Override
    protected boolean doShow() {

        System.out.println(headline() + "\n");

        if (theController.cashierState() == CashierState.CLOSED) {
            System.out.println("There are no opened shifts at the moment.\n\n");
            return false;
        }

        Map<Dish, Integer> dishCookedQuant;
        dishCookedQuant = theController.numAvailableQuantsMeal();
        if (dishCookedQuant.isEmpty()) {
            System.out.println("There are no available meals for this shift.\n\n");
            return false;

        }

        for (Dish d : dishCookedQuant.keySet()) {
            System.out.println(d.toString() + "Available quantity:" + dishCookedQuant.get(d));
        }

        System.out.println("\n");
        return false;
    }

    @Override
    public String headline() {
        return "Cooked quantity for each available dish.";
    }

}
