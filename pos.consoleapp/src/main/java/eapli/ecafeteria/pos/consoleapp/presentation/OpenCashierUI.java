/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.ecafeteria.application.cashier.OpenCashierController;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

/**
 *
 * @author Pedro
 */
public class OpenCashierUI extends AbstractUI {

    private final Scanner in = new Scanner(System.in);

    private final String MEALTYPE_HEADER = "\nSelect the meal type.";

    private final String EXIT_MESSAGE = "\nThe operation was canceled.";

    private final String DATA_HEADER = "\nSelect the data input.";
    /**
     * A CloseCashierController
     */
    private final OpenCashierController ccont = new OpenCashierController();

    /**
     * Returns the controller used by this class
     *
     * @return the controller
     */
    protected Controller controller() {
        return this.ccont;
    }

    @Override
    protected boolean doShow() {
        Calendar dateCalendar = null;
        Calendar hourCalendar = null;
        boolean success = false;
        
        try {
            ccont.startOpenCashierOperation();
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
            System.out.println(EXIT_MESSAGE);
            return false;
        }
        boolean acceptedDate = false;
        ArrayList<String> dataChoice = new ArrayList<>();
        dataChoice.add("Default (today)");
        dataChoice.add("Other");
        SelectWidget<String> dateWidget = new SelectWidget<>(DATA_HEADER, dataChoice);
        dateWidget.show();
        String choice = dateWidget.selectedElement();
        if (choice == null) {
            System.out.println(EXIT_MESSAGE);
            return false;
        }
        do {
            if (choice.equals("Default (today)")) {
                dateCalendar = DateTime.now();
                hourCalendar = DateTime.now();
                DateTime.format(hourCalendar, "HH:mm");
                acceptedDate = true;
            } else {
                System.out.println("\nIntroduce the date of the shift to work on with the format 'dd-mm-yyyy'. To exit type '0'.");
                String date = in.nextLine();
                if (date.equals("0")) {
                    System.out.println(EXIT_MESSAGE);
                     return false;
                }
                System.out.println("\nIntroduce the hour of the shift you want with format 'hh:mm'. To exit type '0'.");
                String hour = in.nextLine();
                if (hour.equals("0")) {
                    System.out.println(EXIT_MESSAGE);
                    return false;
                }
                if (date.matches("(([0-2][0-9])|3[0|1])-(0[1-9]|1[0-2])-[0-9]{4}") &&
                        hour.matches("([0-1][0-9]|2[0-4]):([0-5][0-9]|60)")) {
                    dateCalendar = DateTime.parseDate(date);
                    hourCalendar = DateTime.parseDate(hour, "HH:mm");
                    if (dateCalendar != null && hourCalendar != null) {
                        acceptedDate = true;
                    }
                }
                
            }
        } while (!acceptedDate);

        try {
            success = ccont.openCashierShift(dateCalendar, hourCalendar);
        } catch (DataConcurrencyException ex) {
            System.out.println("The cashier you are trying to modify is not updated");
        } catch (DataIntegrityViolationException ex) {
            System.out.println("The data introduzed to modify the cashier wasn't correct");
        }

        if (success = true) {
            System.out.println("Open cashier operation successfull");
            return true;
        }
        System.out.println("An error as ocurred during the opening of the cashier");
        return false;
    }

    @Override
    public String headline() {
        return "Open Cashier";
    }

}
