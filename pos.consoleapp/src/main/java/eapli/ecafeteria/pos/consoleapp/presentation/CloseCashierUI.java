package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.cafeteria.consoleapp.presentation.authz.LogoutAction;
import eapli.ecafeteria.application.cashier.CloseCashierController;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1150416
 */
public class CloseCashierUI extends AbstractUI
{
    /**
     * A CloseCashierController
     */
    private final CloseCashierController ccont = new CloseCashierController();

    /**
     * Returns the controller used by this class
     * @return the controller
     */
    protected Controller controller(){
        return this.ccont;
    }
    
    /**
     * Method that prints the ungathered meals.
     * @return true if succeeded, false if failed
     */
    @Override
    protected boolean doShow() {
        boolean success = true;
        ArrayList<String> mealNames = new ArrayList<>();
        try {
            mealNames = ccont.startCloseCashierOperation();
        
        ArrayList<Integer> mealNumber = ccont.countMeals();
        
        if(mealNames.isEmpty() || mealNumber == null){
            System.out.println("There are no meals!");
            success = false;
        }else{
            for (int i = 0; i < mealNames.size() ; i++) {
                System.out.println(mealNames.get(i) +" "+mealNumber.get(i));
            }
        }
        try {
            ccont.executeLogout();
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            System.out.println("Error Ocurred while closing the cashier!");
        }
        
        new LogoutAction().execute();
        
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            Logger.getLogger(CloseCashierUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return success;
    }

    @Override
    public String headline() {
        return "Close Cashier";
    }
    
}
