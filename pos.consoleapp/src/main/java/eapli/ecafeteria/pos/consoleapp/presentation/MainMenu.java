/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.cafeteria.consoleapp.presentation.MyUserMenu;
import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.framework.actions.ReturnAction;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.HorizontalMenuRenderer;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.ShowVerticalSubMenuAction;
import eapli.framework.presentation.console.SubMenu;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.presentation.console.VerticalSeparator;

/**
 * TODO split this class in more specialized classes for each menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    private static final int EXIT_OPTION = 0;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int CASHIER_OPTION = 2;
    private static final int SALES_CHARGING_OPTION = 3;

    //CASHIER
    private static final int OPEN_SHIFT_OPTION = 1;
    private static final int CLOSE_SHIFT_OPTION = 2;
    private static final int RELOAD_CARD_OPTION = 3;

    //SALES CHARGING
    private static final int REGISTER_BOOKING_DELIVERY_OPTION = 1;
    private static final int CARD_CHARGING_OPTION = 2;

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer;
        if (Application.settings().isMenuLayoutHorizontal()) {
            renderer = new HorizontalMenuRenderer(menu);
        } else {
            renderer = new VerticalMenuRenderer(menu);
        }
        return renderer.show();
    }

    @Override
    public String headline() {
        new ViewAvailableQuantsMealAction().execute();
        return "eCafeteria POS [@" + Application.session().session().authenticatedUser().id() + "]";
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.add(new SubMenu(MY_USER_OPTION, myUserMenu, new ShowVerticalSubMenuAction(myUserMenu)));

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }

        if (Application.session().session().authenticatedUser().isAuthorizedTo(ActionRight.MANAGE_DELIVERY)) {

            final Menu cashierMenu = buildCashierMenu();
            mainMenu.add(new SubMenu(CASHIER_OPTION, cashierMenu, new ShowVerticalSubMenuAction(cashierMenu)));

//            mainMenu.add(new MenuItem(MY_USER_OPTION_2, "Booking Delivery", new ExitWithMessageAction()));
//            mainMenu.add(new MenuItem(MY_USER_OPTION_3, "Carregar o cartão", new ExitWithMessageAction()));
//            mainMenu.add(new MenuItem(MY_USER_OPTION_4, "Close cash", new ExitWithMessageAction()));
//            mainMenu.add(new MenuItem(MY_USER_OPTION_5, "Change Password", new ExitWithMessageAction()));
//            mainMenu.add(new MenuItem(MY_USER_OPTION_6, "Occorences/Reclamacoes", new ExitWithMessageAction()));
        }

        if (Application.session().session().authenticatedUser().isAuthorizedTo(ActionRight.SALE)) {

            final Menu salesMenu = buildSalesMenu();
            mainMenu.add(new SubMenu(SALES_CHARGING_OPTION, salesMenu, new ShowVerticalSubMenuAction(salesMenu)));

        }

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }

        mainMenu.add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));

        return mainMenu;
    }

    private Menu buildCashierMenu() {
        final Menu menu = new Menu("Cashier >");
        menu.add(new MenuItem(OPEN_SHIFT_OPTION, "Open Shift", new OpenCashierAction()));
        menu.add(new MenuItem(CLOSE_SHIFT_OPTION, "Close Shift", new CloseCashierAction()));
        menu.add(new MenuItem(RELOAD_CARD_OPTION, "Reload card", new ReloadCardAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }

    private Menu buildSalesMenu() {
        final Menu menu = new Menu("Sales and Charging >");
        menu.add(new MenuItem(REGISTER_BOOKING_DELIVERY_OPTION, "Register booking delivery", new RegisterReservationDeliveryAction()));
//        menu.add(new MenuItem(CARD_CHARGING, "Card charge", new DeactivateUserAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }

}
