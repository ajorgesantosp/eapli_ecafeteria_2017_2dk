package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.framework.actions.Action;

/**
 *
 * @author 1150416
 */
public class CloseCashierAction implements Action{

    @Override
    public boolean execute() {
        return new CloseCashierUI().doShow();
    }
    
}
