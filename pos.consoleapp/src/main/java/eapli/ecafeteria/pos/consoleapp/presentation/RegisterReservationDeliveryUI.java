/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.ecafeteria.application.cashier.RegisterReservationDeliveryController;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fabio e cesar
 */
public class RegisterReservationDeliveryUI extends AbstractUI {

    private final RegisterReservationDeliveryController theController = new RegisterReservationDeliveryController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        String fileName;
        String filePath;
        String qrcodeInformation = "";
        boolean loop = true;
        boolean qrRead;
        boolean bookingPass;

        try {
            theController.verifyStateofCashier();
        } catch (IllegalStateException ex) {
            System.out.println(ex.getMessage());
            return false;
        }

        try {
            fileName = Console.readLine("Please insert the name of the QRCode (Insert \"exit\" to leave):");
            if (fileName.equalsIgnoreCase("exit")) {
                return false;
            }
            filePath = "../Resources/qrcodes/" + fileName + ".png";
            qrcodeInformation = theController.getReservationInfoFromQRCodeTag(filePath);
        } catch (NullPointerException | FileNotFoundException ex) {
            System.out.println("Please choose a valid path to the image!");

        } catch (IOException ex) {
            Logger.getLogger(RegisterReservationDeliveryUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        qrRead = theController.searchUserReservation(qrcodeInformation);

        if (qrRead) {
            bookingPass = theController.verifyBookingIntegrity();
        } else {
            System.out.println("The booking read doesn't exist!");
            return false;
        }

        System.out.println("Information of the booking:\n");
        System.out.println(theController.getBookingData() + "\n");

        if (!bookingPass) {
            System.out.println("ATTENTION: The booking presented isn't for this day or for the current Type of Meal !!");
        }

        while (loop) {
            String res = Console.readLine("You want to confirm the delivery of reserve?(Y (YES) or N (NO)");
            if (res.equalsIgnoreCase("y") || res.equalsIgnoreCase("yes")) {
                try {
                    theController.changeReservationState();
                    loop = false;
                } catch (DataIntegrityViolationException | DataConcurrencyException ex) {
                    Logger.getLogger(RegisterReservationDeliveryUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (res.equalsIgnoreCase("n") || res.equalsIgnoreCase("no")) {
                return false;
            } else {
                System.out.println("Please consider only the choices Y(Yes) or N(No)");
            }
        }
        System.out.println("Booking delivered successfully!");
        return true;
    }

    @Override
    public String headline() {
        return "Register Reservation Delivery";
    }
}
