/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.framework.actions.Action;

/**
 *
 * @author 1150450 and 1150585
 */
public class ViewAvailableQuantsMealAction implements Action {

    @Override
    public boolean execute() {
        return new ViewAvailableQuantsMealUI().doShow();
    }

}
