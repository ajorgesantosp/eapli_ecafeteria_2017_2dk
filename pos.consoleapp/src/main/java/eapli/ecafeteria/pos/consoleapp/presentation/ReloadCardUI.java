/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.ecafeteria.application.cashier.ReloadCardController;
import eapli.ecafeteria.domain.authz.CardMovements;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.delivery.Cashier;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;

/**
 *
 * @author Esquilo
 */
public class ReloadCardUI extends AbstractUI{
    
 private Scanner read= new Scanner(System.in);
    private double amount;
    private ReloadCardController RCcontroller;
    
    

    public ReloadCardUI() {
        this.RCcontroller= new ReloadCardController();
        
    }
    
    
  

    @Override
    protected boolean doShow() {
        while(RCcontroller.getUser()==null){
        System.out.println("Indicate the mecanographicNumber ");
    
        String number =read.next();
        MecanographicNumber mn = new MecanographicNumber(number);
    
    try{
        RCcontroller.getUserByNumber(mn);
    }catch (NoResultException ex){
        System.out.println("The user does not exists");       
    }
    }
    amount=-1;
    while(amount <=0){
        System.out.println("Indicate the amount is wish to recharge");
        amount=read.nextDouble();
     try {
         System.out.println(RCcontroller.generateMBReference(amount)+"\n");
     } catch (Cashier.NegativeAmountException ex) {
     }
    }
            
           
            
           
            
             System.out.println("Paid confirmation?(yes or no)");
               String answer = read.next();
             
             
             if(answer.equalsIgnoreCase("yes")){
                 
                try {
                    RCcontroller.confirmPayment();
                    RCcontroller.saveUser();
                    RCcontroller.saveMovement();
                    
                } catch (DataConcurrencyException ex) {
                    Logger.getLogger(ReloadCardUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (DataIntegrityViolationException ex) {
                    Logger.getLogger(ReloadCardUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (CardMovements.NegativeAmountException ex) {
                Logger.getLogger(ReloadCardUI.class.getName()).log(Level.SEVERE, null, ex);
            }

                }
             
            
                 
                    return true;
                 
             }
    

    @Override
    public String headline() {
         return "Reload Card";
    }
            
            
            }
    

