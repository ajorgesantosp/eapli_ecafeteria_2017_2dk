package eapli.ecafeteria.domain.delivery;

import java.io.Serializable;

/**
 *
 *
 * @author nunom
 */
public enum CashierState implements Serializable {

    OPEN {
        private final String id = "Open";

        @Override
        public CashierState state() {
            return OPEN;
        }

        @Override
        public String toString() {
            return id;
        }

    },
    CLOSED {
        private final String id = "Closed";

        @Override
        public CashierState state() {
            return CLOSED;
        }

        @Override
        public String toString() {
            return id;
        }
    };

    public abstract CashierState state();

}
