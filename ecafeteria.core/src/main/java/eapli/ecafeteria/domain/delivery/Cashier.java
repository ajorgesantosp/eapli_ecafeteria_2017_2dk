/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.delivery;

import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.domain.ddd.AggregateRoot;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author nunom
 */
@Entity
@Table(name = "CASHIER")
public class Cashier implements Serializable, AggregateRoot<Long> {

    //Cashier State(open or close)
    @Enumerated(EnumType.STRING)
    public CashierState state;
    @OneToOne
    public Shift currentShift;
    @OneToMany(cascade = CascadeType.MERGE)
    public List<Shift> historicoTurnos;
    @Id
    private Long cashierID;

    public Cashier() {
        this.currentShift = null;
        historicoTurnos = new ArrayList<>();
        state = CashierState.CLOSED;
        cashierID = new Long(1);
    }

    public boolean openCashier(Calendar date, List<Meal> lstMeal) {
        if (state == CashierState.OPEN) {
            return false;
        }
        state = CashierState.OPEN;
        this.currentShift = new Shift(date, lstMeal);
        return true;
    }

    /**
     * Method that changes the state of the cashier.
     */
    public void closeCashier() {
        state = CashierState.CLOSED;
        BookingRepository bookingRepositoryUnit = PersistenceContext.repositories().bookings();
        for (Meal m : meals()) {
            ArrayList<Booking> bookings = (ArrayList<Booking>) bookingRepositoryUnit.findBookingsByMealAndState(Booking.States.Reserved, m);
            for (Booking b : bookings) {
                b.changeState(Booking.States.Undelivered);
            }
        }
        this.historicoTurnos.add(currentShift);
    }

    public Shift currentShift() {
        if (state.equals(CashierState.OPEN)) {
            return this.currentShift;
        }
        return null;
    }

    public CashierState state() {
        return this.state;
    }

    public List<Meal> meals() {
        return currentShift.mealList();
    }

    public String generateMBreference(double amount) throws NegativeAmountException {

        if (amount <= 0) {
            throw new NegativeAmountException();
        }
        Random ran = new Random();
        int x = ran.nextInt(99999999);
        return ("Reference to charge \n "
                + "Cafeteria entity: 12435 \n"
                + "Reference : " + x + "\n"
                + "Amount:" + amount);
    }

    public void confirmPayment(CafeteriaUser user, double amount) {
        user.cardAccount().addFunds(amount);

    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Cashier)) {
            return false;
        }

        final Cashier that = (Cashier) other;

        if (this == that) {
            return true;
        }

        if (!this.cashierID.equals(that.cashierID)) {
            return false;
        }

        if (!this.state.equals(that.state)) {
            return false;
        }

        if (!this.currentShift.equals(that.currentShift)) {
            return false;
        }

        if (!this.historicoTurnos.equals(that.historicoTurnos)) {
            return false;
        }

        return true;
    }

    @Override
    public boolean is(Long id) {
        return id().equals(id);
    }

    @Override
    public Long id() {
        return cashierID;
    }

    /**
     * This method goes into every meal stored in the cashier and returns their
     * names as a List of Strings
     *
     * @return List - the name of the meals
     */
    public List<String> nameOfMealsServed() {
        ArrayList<String> lstMealName = new ArrayList<>();
        //If there are no meals, return the empty list
        if (meals().isEmpty()) {
            return lstMealName;
        }
        //go through every meal and saves its' name
        for (Meal m : meals()) {
            lstMealName.add(m.dish().name().toString());
        }
        return lstMealName;
    }

    /**
     * A method that receives bookings and compares them to the meals in the
     * cashier and subtractes for a meal, the number of booked meals - the
     * number of gathered meals
     *
     * @return an Integer List with the calculated diff for each meal in Cashier
     */
    public List<Integer> countBookingDiffOfMeals() throws DataConcurrencyException, DataIntegrityViolationException {
        BookingRepository bookingRepositoryUnit = PersistenceContext.repositories().bookings();
        ArrayList<Integer> mealsCount = new ArrayList<>();
        ArrayList<Booking> bookings = new ArrayList<>();

        for (Meal m : meals()) {
            bookings = (ArrayList<Booking>) bookingRepositoryUnit.findBookingsByMealAndState(Booking.States.Reserved, m);
            mealsCount.add(bookings.size());
            for (Booking b : bookings) {
                b.changeState(Booking.States.Undelivered);
                bookingRepositoryUnit.save(b);
            }
        }
        return mealsCount;
    }

    /**
     * Checks if the state of the cashier is equals the one received
     *
     * @param state the state to check
     * @return true if the states they are the same, false if the states aren't
     * equal
     */
    public boolean checkState(CashierState state) {
        if (state.equals(this.state)) {
            return true;
        }
        return false;
    }

    public static class NegativeAmountException extends Exception {

        public NegativeAmountException() {
            System.out.println("The amount must be a positive number and bigger than 0");
        }
    }

}
