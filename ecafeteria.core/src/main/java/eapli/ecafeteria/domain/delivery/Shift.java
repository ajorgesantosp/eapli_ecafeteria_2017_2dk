/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.delivery;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.domain.ddd.AggregateRoot;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author nunom
 */
@Entity
@Table(name = "SHIFT")
public class Shift implements Serializable, AggregateRoot<Long> {

    @OneToMany(fetch = FetchType.EAGER)
    private ArrayList<Meal> mealList;

    private Calendar date;
    @Id
    @GeneratedValue
    private Long id;

    protected Shift() {
        mealList = new ArrayList<>();
    }

    public Shift(Calendar date, List<Meal> mealList) {
        this.date = date;
        this.mealList = (ArrayList<Meal>) mealList;
    }

    /**
     * @return the mealList
     */
    public List<Meal> mealList() {
        return mealList;
    }

    /**
     * @return the date
     */
    public Calendar date() {
        return date;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Shift)) {
            return false;
        }

        final Shift that = (Shift) other;
        if (this == that) {
            return true;
        }

        if (!this.id.equals(that.id)) {
            return false;
        }

        if (!this.date.equals(that.date)) {
            return false;
        }

        if (!this.mealList.equals(that.mealList)) {
            return false;
        }

        return true;
    }

    @Override
    public boolean is(Long id) {
        return id().equals(id);
    }

    @Override
    public Long id() {
        return id;
    }

}
