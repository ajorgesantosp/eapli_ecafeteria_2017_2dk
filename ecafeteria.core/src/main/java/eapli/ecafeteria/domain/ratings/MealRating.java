package eapli.ecafeteria.domain.ratings;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.domain.ddd.AggregateRoot;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * A rating system with stars (1 - 5) should be provided to evaluate the quality of a certain meal. 
 * Comments should also be allowed, considering they are optional.
 * 
 * @author 1150585 / 1150801
 */
@Entity
// Sets the meal and user of each booking as unique, so that the same user isn't able to book the same meal multiple times
@Table(uniqueConstraints = @UniqueConstraint(columnNames={"NUMBER", "MEALTYPE","DATE"}))
public class MealRating implements Serializable, AggregateRoot<Long> {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    private CafeteriaUser user;

    private int ratingNumber;

    @Temporal(TemporalType.DATE)
    private Date date;

    @Enumerated(EnumType.STRING)
    private MealType mealType;

    public MealRating(CafeteriaUser user, Date date, MealType mealType, int ratingNumber) {
        if (user == null || date == null || mealType == null) {
            throw new IllegalStateException();
        }

        this.user = user;
        this.date = date;
        this.mealType = mealType;
        setRating(ratingNumber);

    }

    public MealRating() {
    }

    /**
     * Sets and validates newRating
     *
     * @param newRating
     */
    private void setRating(int newRating) {
        if (ratingNumberMeetsMinimumRequirements(newRating)) {
            this.ratingNumber = newRating;
        } else {
            throw new IllegalArgumentException("Invalid Rating!");
        }
    }

    /**
     * Ensure Rating is within bounds.
     *
     * @param newRating
     * @return True if newRating meets minimum requirements. False if newRating
     * does not meet minimum requirements.
     */
    private boolean ratingNumberMeetsMinimumRequirements(int newRating) {
        if (newRating > 5) {
            throw new NumberFormatException("Please insert a positive number between 1 and 5!");
        } else if (newRating < 1) {
            throw new NumberFormatException("Please insert a positive number between 1 and 5!");
        } else {
            return true;
        }
    }

    public CafeteriaUser user() {
        return user;
    }

    public Date date() {

        return date;
    }

    public MealType mealType() {

        return mealType;
    }

    public int ratingNumber() {

        return ratingNumber;
    }

    @Override
    public Long id() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final MealRating other = (MealRating) obj;
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }

        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.mealType, other.mealType)) {
            return false;
        }
        if (!Objects.equals(this.ratingNumber, other.ratingNumber)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.user);
        hash = 29 * hash + this.ratingNumber;
        hash = 29 * hash + Objects.hashCode(this.date);
        hash = 29 * hash + Objects.hashCode(this.mealType);
        return hash;
    }

    @Override
    public String toString() {
        return "Meal Rating{" + "Date=" + date + ", Meal Type= " + mealType + ", Rating number= " + ratingNumber + '}';
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof MealRating)) {
            return false;
        }

        final MealRating that = (MealRating) other;
        if (this == that) {
            return true;
        }

        if (!this.id.equals(that.id)) {
            return false;
        }
        if (!this.user.equals(that.user)) {
            return false;
        }
        if (!this.mealType.equals(that.mealType)) {
            return false;
        }
        if (!this.date.equals(that.date)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean is(Long id) {
        return id().equals(id);
    }

}
