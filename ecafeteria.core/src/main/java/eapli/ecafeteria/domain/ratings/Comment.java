package eapli.ecafeteria.domain.ratings;

import eapli.framework.domain.ddd.AggregateRoot;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * A comment is associated with a rating of an already consumed meal.
 * 
 * @author 1150585 / 1150801
 */
@Entity
@Table(name = "COMMENT")
public class Comment implements Serializable, AggregateRoot<Long>{

    @Id
    @GeneratedValue
    private Long idComment;
    
    private String comment;

    @OneToOne
    private MealRating idRating;

    public Comment(String comment, MealRating idRating) {

        if (idRating == null || comment == null) {
            throw new IllegalStateException();
        }

        this.comment = comment;
        this.idRating = idRating;

    }

    public Comment() {

    }

    private String comment() {

        return this.comment;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final Comment other = (Comment) obj;
        if (!Objects.equals(this.comment, other.comment)) {
            return false;
        }

        if (!Objects.equals(this.idRating, other.idRating)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Comment(comment:" + comment + " id rating" + idRating + ")";
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Comment)) {
            return false;
        }

        final Comment that = (Comment) other;
        if (this == that) {
            return true;
        }

        if (!this.idComment.equals(that.idComment)) {
            return false;
        }
        if (!this.comment.equals(that.comment)) {
            return false;
        }
        if (!this.idRating.equals(that.idRating)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean is(Long id) {
        return id().equals(id);
    }

    @Override
    public Long id() {
        return this.idComment;
    }

}
