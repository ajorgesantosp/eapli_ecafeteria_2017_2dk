package eapli.ecafeteria.domain.meals;

/**
 *
 * @author 1150416
 */
public class MealPlanItem {
    /**
    * The booked meals of the meal plan item
    */
    private Integer bookedMeals;
    
    /**
    * The meals that were delivered from the meal of this plan item
    */
    private Integer deliveredMeals;
    
    /**
     * Method that gathers the booked meals of an meal plan item
     * @return Integer booked meals
     */
    public Integer getBookedMealNumber() {
        return bookedMeals;
    }

    /**
     * Method that gathers the delivered meals of and meal plan item
     * @return Integer delivered meals
     */
    public Integer getDeliveredMeal() {
        return deliveredMeals;
    }

}
