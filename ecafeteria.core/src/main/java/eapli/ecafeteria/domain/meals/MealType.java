/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.util.DateTime;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;

/**
 *
 * Created by 1150450 on 02/05/2017.
 */
public enum MealType implements Serializable {

    @Column(name = "MealType")
    LUNCH {
        private final String id = "Lunch";

        @Override
        public String toString() {
            return id;
        }

    },
    DINNER {
        private final String id = "Dinner";

        @Override
        public String toString() {
            return id;
        }
    };

    public static Iterable<MealType> mealTypes() {
        return Arrays.asList(MealType.values());
    }

    public static MealType mealType(String mealType) {
        switch (mealType) {
            case "LUNCH":
                return LUNCH;
            case "DINNER":
                return DINNER;
            default:
                return null;
        }
    }

    public Date dateMealType() {
        Date date = new Date();
        switch (this) {
            case LUNCH:
                date.setTime(12);
                return date;
            case DINNER:
                date.setTime(19);
                return date;
            default:
                return null;
        }
    }

    /**
     * Returns the meal type according to the hour passed by parameter. Between 
     * 3 am and 15 pm, returns LUNCH. Otherwise returns DINNER.
     * @param hourCalendar the hour to check
     * @return a MealType
     */
    public static MealType mealTypeByHour(Calendar hourCalendar){
        if(hourCalendar.after(DateTime.parseDate("15:00","HH:mm")) ||
                hourCalendar.before(DateTime.parseDate("03:00","HH:mm"))){
            return MealType.DINNER;
        }
        return MealType.LUNCH;
    }
}
