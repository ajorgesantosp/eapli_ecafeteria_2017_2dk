/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author nunom
 */
@Embeddable
public class CookedQuantity implements Serializable {

    private int cookedQuantity;

    public CookedQuantity() {
        this.cookedQuantity = 0;
    }

    /**
     * @param cookedQuantity the cookedQuantity to set
     */
    public void changeCookedQuantity(int cookedQuantity) {
        if (cookedQuantity < 0) {
            throw new IllegalArgumentException();
        }
        this.cookedQuantity = cookedQuantity;
    }

    public int cookedQuantity() {
        return this.cookedQuantity;
    }
}
