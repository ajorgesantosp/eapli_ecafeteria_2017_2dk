/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;

/**
 *
 * Created by 1150450 and 1150444 on 02/05/2017.
 */
@Embeddable
public class MealID implements Serializable {

    private static final long serialVersionUID = 1L;

    @GeneratedValue
    private Long mealID;

    protected MealID() {
        // for ORM
    }

    @Override
    public int hashCode() {
        return this.mealID.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MealID other = (MealID) obj;
        if (!Objects.equals(this.mealID, other.mealID)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "" + this.mealID;
    }

}
