/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.ddd.Factory;
import java.util.Calendar;

/**
 *
 * Created by 1150450 and 1150444 on 02/05/2017.
 */
public class MealBuilder implements Factory<Meal> {

    private MealType mealType;
    private Dish dish;
    private Calendar date;
    private int expectedQuantity = 100;

    public MealBuilder() {

    }

    public MealBuilder withMealType(MealType mealType) {
        this.mealType = mealType;
        return this;
    }

    public MealBuilder withDish(Dish dish) {
        this.dish = dish;
        return this;
    }

    public MealBuilder withDate(Calendar date) {
        this.date = date;
        return this;
    }

    @Override
    public Meal build() {
        return new Meal(mealType, dish, date, expectedQuantity);
    }

}
