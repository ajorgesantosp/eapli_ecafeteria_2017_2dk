/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.ddd.AggregateRoot;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author diogo
 */
@Entity
public class Menu implements AggregateRoot<Long>, Serializable {

    private static final long serialVersionUID = 1L;

    // ORM primary key
    @Id
    @GeneratedValue
    private Long menuID;
    @Version
    private Long version;

    @Column(unique = true)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar startDate;
    @Column(unique = true)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar finishDate;

    @ElementCollection
    @CollectionTable(name = "Menu_Meal")
    @Column(name = "Meal")
    @ManyToOne
    private List<Meal> mealList;

    @Enumerated(EnumType.STRING)
    private MenuState menuState;

    protected Menu() {
        // for ORM
    }

    public Menu(Calendar startDate, Calendar finishDate) {
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.mealList = new ArrayList<>();
        this.menuState = MenuState.IN_PROGRESS;
    }

    public Calendar startDate() {
        return this.startDate;
    }

    public Calendar finishDate() {
        return this.finishDate;
    }

    public List<Meal> mealList() {
        return this.mealList;
    }

    public boolean addMeal(Meal meal) {
        if (mealList.contains(meal)) {
            return false;
        }
        return mealList.add(meal);
    }

    public boolean removeMeal(Meal meal) {
        if (!mealList.contains(meal)) {
            return false;
        }
        return mealList.remove(meal);
    }

    public boolean containsMeal(Meal m) {
        return mealList.contains(m);
    }

    public boolean menuPublished() {
        if (!isInProgress()) {
            return false;
        }
        menuState = MenuState.PUBLISHED;
        return true;
    }

    public boolean isInProgress() {
        return this.menuState.equals(MenuState.IN_PROGRESS);
    }

    @Override
    public int hashCode() {
        return this.startDate.hashCode();
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Menu)) {
            return false;
        }

        final Menu that = (Menu) other;
        if (this == that) {
            return true;
        }

        return id().equals(that.id());
    }

    @Override
    public boolean is(Long id) {
        return id().equals(id);
    }

    @Override
    public Long id() {
        return this.menuID;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final Menu other = (Menu) obj;
        if (!Objects.equals(this.startDate, other.startDate)) {
            return false;
        }

        if (!Objects.equals(this.finishDate, other.finishDate)) {
            return false;
        }
        if (!Objects.equals(this.menuID, other.menuID)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        if (!Objects.equals(this.menuState, other.menuState)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf= new SimpleDateFormat("dd-MM-yyyy");
        String str1=sdf.format(startDate.getTime());
        String str2= sdf.format(finishDate.getTime());
        return "startDate=" + str1 + ", finishDate=" + str2;
    }

}
