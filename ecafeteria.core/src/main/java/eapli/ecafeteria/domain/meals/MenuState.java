/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import javax.persistence.*;

/**
 *
 * @author diogo
 */
@Embeddable
public enum MenuState {

    IN_PROGRESS {

        @Override
        public String toString() {
            return "In Progress";
        }

    },
    PUBLISHED {

        @Override
        public String toString() {
            return "In Progress";
        }

    };

}
