/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.framework.domain.ddd.AggregateRoot;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 *
 * Created by 1150450 and 1150444 on 02/05/2017.
 */
@Entity
public class Meal implements Serializable, AggregateRoot<Long> {

    private static final long serialVersionUID = 1L;

//    @EmbeddedId
//    private MealID mealID;
    @Id
    @GeneratedValue
    private Long mealID;

    @Version
    private Long version;

    @Enumerated(EnumType.STRING)
    private MealType mealType;

    @ManyToOne(cascade = CascadeType.ALL)
    private Dish dish;
    @Temporal(TemporalType.DATE)
    private Calendar date;
    @Embedded
    private CookedQuantity cookedQuantity;
    private int expectedQuantity;

    @OneToMany
    @JoinTable(
            name = "MEAL_BATCH",
            joinColumns = @JoinColumn(name = "MEAL_ID", referencedColumnName = "mealID"),
            inverseJoinColumns = @JoinColumn(name = "BATCH_ID", referencedColumnName = "pk")
    )
    private List<Batch> batches;

    protected Meal() {
        this.batches = new ArrayList<>();
    }

    public Meal(MealType mealType, Dish dish, Calendar date, int expectedQuantity) {
        if (mealType == null || dish == null || date == null) {
            throw new IllegalStateException();
        }
        this.mealType = mealType;
        this.dish = dish;
        this.date = date;
        this.batches = new ArrayList<>();
        this.cookedQuantity = new CookedQuantity();
        this.expectedQuantity = expectedQuantity;
    }

    public Dish dish() {
        return this.dish;
    }

    /**
     * @return the meal type
     */
    public MealType mealType() {
        return mealType;
    }

    /**
     * The current date of meal
     *
     * @return The current date of meal
     */
    public Calendar date() {
        return date;
    }

    public List<Batch> batches() {
        return batches;
    }

    public void changeBatches(List<Batch> batches) {
        this.batches = batches;
    }

    public boolean addBatch(Batch batch) {
        return batches.add(batch);
    }

    public boolean removeBatch(Batch batch) {
        return batches.remove(batch);
    }

    public CookedQuantity cookedQuantity() {
        return this.cookedQuantity;
    }

    @Override
    public int hashCode() {
        return this.mealID.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Meal other = (Meal) obj;
        if (!Objects.equals(this.mealID, other.mealID)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        if (this.mealType != other.mealType) {
            return false;
        }
        if (!Objects.equals(this.dish, other.dish)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.cookedQuantity, other.cookedQuantity)) {
            return false;
        }
        if (!Objects.equals(this.batches, other.batches)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Meal)) {
            return false;
        }

        final Meal that = (Meal) other;
        if (this == that) {
            return true;
        }

        return mealType().equals(that.mealType()) && dish().equals(that.dish())
                && date().equals(that.date());
    }

    @Override
    public String toString() {
        return "MealType: " + mealType + ", " + dish;
    }

    @Override
    public boolean is(Long id) {
        return id().equals(id);
    }

    @Override
    public Long id() {
        return this.mealID;
    }

}
