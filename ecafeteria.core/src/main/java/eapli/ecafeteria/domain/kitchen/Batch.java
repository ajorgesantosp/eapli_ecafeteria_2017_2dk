/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import eapli.framework.domain.ddd.AggregateRoot;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 *
 * @author ismal
 */
@Entity
public class Batch implements AggregateRoot<Long>, Serializable {

    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;

    @Column(unique = true)
    private String acronym;
    @Column
    private Integer quantity;
    private Material ingredient;
    private String description;

    public Batch() {
    }

    public Batch(Material ingredient, String acronym, String description, Integer quantity) {
        this.ingredient = ingredient;
        this.acronym = acronym;
        this.description = description;
        this.quantity = quantity;
    }

    public String acronym() {
        return acronym;
    }

    public String description() {
        return description;
    }

    public Material ingredient() {
        return ingredient;
    }

    public Integer quantity() {
        return quantity;
    }

    public void changeDescription(String newDescription) {
        this.description = newDescription;
    }

    public void changeIngredient(Material newIngredient) {
        this.ingredient = newIngredient;
    }

    public void changeQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean sameAs(Object obj) {
        return this.equals(obj);

    }

    @Override
    public boolean is(Long id) {
        return id().equals(id);
    }

    @Override
    public Long id() {
        return this.pk;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Batch other = (Batch) obj;
        if (!Objects.equals(this.acronym, other.acronym)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.pk, other.pk)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        if (!Objects.equals(this.quantity, other.quantity)) {
            return false;
        }
        if (!Objects.equals(this.ingredient, other.ingredient)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Batch{" + "acronym=" + acronym + ", quantity=" + quantity + ", ingredient=" + ingredient + ", description=" + description + '}';
    }
    

}
