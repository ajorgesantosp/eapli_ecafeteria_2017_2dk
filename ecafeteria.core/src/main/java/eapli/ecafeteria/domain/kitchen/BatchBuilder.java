/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import eapli.framework.domain.ddd.Factory;

/**
 *
 * @author ismal
 */
public class BatchBuilder implements Factory<Batch> {

    private String acronym;
    private Material ingredient;
    private String description;
    private Integer qt;

    public BatchBuilder() {
    }

    public BatchBuilder withAcronym(String newAcronym) {
        this.acronym = newAcronym;
        return this;
    }

    public BatchBuilder withIngredient(Material newIngr) {
        this.ingredient = newIngr;
        return this;
    }

    public BatchBuilder withDescription(String newDescr) {
        this.description = newDescr;
        return this;
    }

    public BatchBuilder withQuantity(Integer qt) {
        this.qt = qt;
        return this;
    }

    @Override
    public Batch build() {
        return new Batch(ingredient, acronym, description, qt);
    }

}
