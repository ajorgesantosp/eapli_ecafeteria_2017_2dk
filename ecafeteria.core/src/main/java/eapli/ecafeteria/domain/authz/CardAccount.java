/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.authz;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

/**
 *
 * @author ismal
 */
@Embeddable
public class CardAccount implements Serializable {

    
    
    private int cardNumber;
    private double balance;
    
    @Transient
    private List<CardMovements> listOfMovements;
 

    public CardAccount(int cardNumber, double balance) {
        this.cardNumber = cardNumber;
        this.balance = balance;
        listOfMovements=new ArrayList<CardMovements>();
    }

    public CardAccount() {
        cardNumber = 0;
        balance = 0;
         listOfMovements=new ArrayList<CardMovements>();
    }

    /**
     * @return the cardNumber
     */
    public int getCardNumber() {
        return cardNumber;
    }

    /**
     * @return the balance
     */
    public double getBalance() {
        return balance;
    }

    /**
     * @param balance the balance to set
     */
    public void setBalance(double balance) {
        this.balance = balance;
    }
    
    public CardAccount getUserByNumber(int mecanographicNumber){
        return null;
    }

    public List<CardMovements> getListOfMovements() {
        return listOfMovements;
    }

    public void setListOfMovements(List<CardMovements> listOfMovements) {
        this.listOfMovements = listOfMovements;
    }
    
    public void addMovement(CardMovements movement){
        listOfMovements.add(movement);
    }
    
    

    
    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.cardNumber;
        hash = 83 * hash + Objects.hashCode(this.balance);

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CardAccount other = (CardAccount) obj;
        if (this.cardNumber != other.cardNumber) {
            return false;
        }

        if (!Objects.equals(this.balance, other.balance)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "CardAccount{" + "cardNumber=" + cardNumber + ", balance=" + balance + '}';
    }
    
    public void subtractFromBalance(double amount) {
        this.balance -= amount;
    }
    
    public void addFunds(double amount){
        
        this.balance=balance+amount;
        
    }
}
