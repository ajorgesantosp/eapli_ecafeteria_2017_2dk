/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.authz;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.framework.domain.ddd.ValueObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 *
 * @author Leandro Santos
 */
@Embeddable
public class Profile implements ValueObject, Serializable {

    private Integer saltQuantWeek;
    private Integer calQuantMeal;
    private Integer calQuantWeek;

    @ElementCollection
    @CollectionTable(name = "Profile_Allergen")
    @Enumerated(EnumType.STRING)
    @Column(name = "Allergen")
    private List<Allergen> allergenList;

    public Profile(Integer saltQuantWeek, Integer calQuantMeal, Integer calQuantWeek) {
        if (saltQuantWeek < 0 || calQuantMeal < 0 || calQuantWeek < 0) {
            throw new IllegalArgumentException();
        }

        this.saltQuantWeek = saltQuantWeek;
        this.calQuantMeal = calQuantMeal;
        this.calQuantWeek = calQuantWeek;
        this.allergenList = new ArrayList<>();
    }

    protected Profile() {
        this.allergenList = new ArrayList<>();
    }

    public Integer saltQuantWeek() {
        return this.saltQuantWeek;
    }

    public Integer calQuantMeal() {
        return this.calQuantMeal;
    }

    public Integer calQuantWeek() {
        return this.calQuantWeek;
    }

    public List<Allergen> allergenList() {
        return allergenList;
    }

    public void changeSaltQuantWeek(Integer saltQuantWeek) {
        if (saltQuantWeek < 0) {
            throw new IllegalArgumentException();
        }
        this.saltQuantWeek = saltQuantWeek;
    }

    public void changeCalQuantMeal(Integer calQuantMeal) {
        if (calQuantMeal < 0) {
            throw new IllegalArgumentException();
        }
        this.calQuantMeal = calQuantMeal;
    }

    public void changeCalQuantWeek(Integer calQuantWeek) {
        if (calQuantWeek < 0) {
            throw new IllegalArgumentException();
        }
        this.calQuantWeek = calQuantWeek;
    }

    public boolean addAllergen(Allergen allergen) {
        if (allergen == null) {
            throw new IllegalStateException();
        }
        if (allergenList.contains(allergen)) {
            return false;
        }
        return allergenList.add(allergen);
    }

    public boolean removeAllergen(Allergen allergen) {
        if (allergen == null) {
            throw new IllegalStateException();
        }
        if (!allergenList.contains(allergen)) {
            return false;
        }
        return allergenList.remove(allergen);
    }

}
