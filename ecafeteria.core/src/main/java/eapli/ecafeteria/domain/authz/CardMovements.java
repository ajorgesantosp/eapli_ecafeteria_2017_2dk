/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.authz;

import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Esquilo
 */
@Entity
public class CardMovements  implements Serializable {
    
    @Id
    @GeneratedValue
    private int id;
    
    private MecanographicNumber mecanographicNumber;
    private double amount;
    private String description;

    public CardMovements() {
    }

    public CardMovements( MecanographicNumber mecanographicNumber, double amount, String description) throws NegativeAmountException {
         if (mecanographicNumber == null || description == null ) {
            throw new IllegalStateException();
        }
       if(amount<=0){
           throw new NegativeAmountException();
           
       }
        this.mecanographicNumber = mecanographicNumber;
        this.amount = amount;
        this.description = description;
    }
 
    
    
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MecanographicNumber getMecanographicNumber() {
        return mecanographicNumber;
    }

    public void setMecanographicNumber(MecanographicNumber mecanographicNumber) {
        this.mecanographicNumber = mecanographicNumber;
    }
    
    public static class NegativeAmountException extends Exception {

        public NegativeAmountException() {
            System.out.println("The amount must be a positive number and bigger than 0");
        }
    }
    
}
