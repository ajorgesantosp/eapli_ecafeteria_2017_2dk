/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.booking;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.domain.ddd.AggregateRoot;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author ismal
 */
@Entity
// Sets the meal and user of each booking as unique, so that the same user isn't able to book the same meal multiple times
@Table(uniqueConstraints = @UniqueConstraint(columnNames={"meal_mealid", "number"}))
public class Booking implements AggregateRoot<Long>, Serializable {

    @Version
    private Long version;

    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne(cascade = CascadeType.MERGE)
    private CafeteriaUser user;
    
    @ManyToOne(cascade = CascadeType.MERGE)
    private Meal meal;
    
    @Enumerated(EnumType.STRING)
    private States state;

    public enum States {
        Reserved, Delivered, Canceled, Undelivered
    }

  public Booking(CafeteriaUser user, Meal meal) {
        this.user = user;
        this.meal = meal;
        this.state = States.Reserved;
     }

    private Booking() {
        user = null;
        meal = null;
    }

    /**
     * @return the user
     */
    public CafeteriaUser user() {
        return user;
    }

    /**
     * @return the meal
     */
    public Meal meal() {
        return meal;
    }

    /**
     * Chagnge the state of the booking
     *
     * @param newState a new state of the booking
     * @return true if state is changed with success
     */
    public boolean changeState(States newState) {
        this.state = newState;
        return true;
    }

    /**
     * @return state of booking
     */
    public States state() {
        return this.state;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.user);
        hash = 41 * hash + Objects.hashCode(this.meal);
        return hash;
    }

    @Override
    public String toString() {
        return "Booking{ user=" + user + ", meal=" + meal + '}';
    }

    @Override
    public boolean sameAs(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Booking other = (Booking) obj;

        return user.equals(other.user) && meal.equals(other.meal);
    }

    @Override
    public boolean is(Long id) {
        return this.id.equals(id);
    }

    @Override
    public Long id() {
        return this.id;
    }

}
