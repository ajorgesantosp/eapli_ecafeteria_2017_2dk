/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.booking;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.domain.ddd.Factory;

/**
 *
 * @author ismal
 */
public class BookingBuilder implements Factory<Booking> {

    private CafeteriaUser user;
    private Meal meal;

    public BookingBuilder() {
    }

    public BookingBuilder withAccount(CafeteriaUser user) {
        this.user = user;
        return this;
    }

    public BookingBuilder withMeal(Meal refeicao) {
        this.meal = refeicao;
        return this;
    }

    @Override
    public Booking build() {
        return new Booking(user, meal);
    }

}
