package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.ratings.Comment;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author 1150585 / 1150801
 */
public interface CommentRepository extends DataRepository<Comment, Long> {
    
}
