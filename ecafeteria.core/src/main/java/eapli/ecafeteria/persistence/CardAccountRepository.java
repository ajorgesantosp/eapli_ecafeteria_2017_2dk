/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.authz.CardAccount;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author ismal
 */
public interface CardAccountRepository extends DataRepository<CardAccount,Long> {
    
    
    CardAccount findByNumber(Integer number);
}
