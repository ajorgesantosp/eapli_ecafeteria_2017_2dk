/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Calendar;

/**
 *
 * Created by 1150450 and 1150444 on 04/05/2017.
 */
public interface MealRepository extends DataRepository<Meal, Long> {

    Meal findByID(Long id);

    /**
     * Returns the meals that have the type and date introduced by the user
     *
     * @param date date of the meal
     * @param type type of meal
     * @return
     */
    Iterable<Meal> mealsByDateAndType(Calendar date, MealType type);

    Iterable<Meal> duplicateMeals(Meal meal);

    Iterable<Meal> mealsByDate(Calendar date);

    Iterable<Meal> allMealsDateInterval(Calendar startDate, Calendar finishDate);

}
