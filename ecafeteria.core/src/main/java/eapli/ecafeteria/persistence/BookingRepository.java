/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author ismal
 */
public interface BookingRepository extends DataRepository<Booking, Long> {

    Iterable<Booking> activeReserves();

    Booking findById(Long id);
    
    Iterable<Booking> bookingReservedOfUserThisDayOn(CafeteriaUser user, Calendar date);
    
    Booking reserveByUserStateMealID(CafeteriaUser user, Booking.States state, Meal meal);
    
    Booking checkIfBookingExists (CafeteriaUser user, Booking.States state, MealType mealType, Date date);
    
    Iterable<Booking> getBookingsByState(Booking.States state);
    
    Iterable<Booking> getBookingsBymealID(Meal m);
    
    Iterable<Booking> findBookingsByMealAndState(Booking.States state, Meal meal);
    
    Iterable<Booking> getBookingForNextDays(CafeteriaUser user,Date currentDay, Date limitDay);
}
