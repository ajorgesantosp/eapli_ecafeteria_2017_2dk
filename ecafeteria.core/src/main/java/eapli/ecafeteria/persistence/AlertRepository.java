/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.kitchen.KitchenAlertManagement;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author 1150425 & 1150482
 */
public interface AlertRepository extends DataRepository<KitchenAlertManagement, String>{
    
    public KitchenAlertManagement findByName(String name);
    
}
