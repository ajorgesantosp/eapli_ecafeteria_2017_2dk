/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.authz.CardMovements;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author Esquilo
 */
public interface CardMovementsRepository extends DataRepository<CardMovements, MecanographicNumber> {
    
    
     CardMovements findByMecanographicNumber(MecanographicNumber number);
}
