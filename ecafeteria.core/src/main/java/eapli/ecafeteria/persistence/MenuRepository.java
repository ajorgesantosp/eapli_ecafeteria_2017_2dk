/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Calendar;

/**
 *
 * @author Leandro Santos
 */
public interface MenuRepository extends DataRepository<Menu, Long> {

    Menu findByDate(Calendar date);

    Iterable<Menu> findMenuInProgress();

    Iterable<Menu> findMenuPublished();

    Menu findByStartFinishDate(Calendar startDate, Calendar finishDate);

    Menu findByStartDate(Calendar startDate);
    
    Menu findByFinishDate(Calendar finishDate);
    Menu menuByDate(Calendar date);

}
