/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlanItem;

/**
 *
 * @author 1150416
 */
public interface MealPlanItemRepository {

    public MealPlanItem getMealPlanItemWithMeal(Meal m);
    
}
