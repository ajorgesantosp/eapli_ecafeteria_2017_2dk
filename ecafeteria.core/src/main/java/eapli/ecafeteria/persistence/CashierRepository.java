package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.delivery.Cashier;
import eapli.framework.persistence.repositories.DataRepository;

/**
 * the repository for Materials.
 *
 */
public interface CashierRepository extends DataRepository<Cashier, Long> {

    public Cashier findById(Long id);
    
}
