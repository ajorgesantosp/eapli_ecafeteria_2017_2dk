/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nunom
 */
public class QuantityMealsCookedController implements Controller {

    private final MealRepository repository = PersistenceContext.repositories().meals();

    private Calendar date;
    private MealType mealType;

    public QuantityMealsCookedController() {
    }

    /**
     *
     * @param date
     * @return
     *
     * Receives a Strings that gets converted to Date
     */
    public boolean insertDate(String date) {
        this.date = DateTime.parseDate(date);
        return this.date != null;
    }

    public Iterable<MealType> allMealTypes() {
        return MealType.mealTypes();
    }

    /**
     * Sets the meal type chosen by the user.
     *
     * @param mealType
     */
    public void insertMealType(MealType mealType) {
        this.mealType = mealType;
    }

    /**
     *
     * @return List of Meals
     *
     * Returns the list of planned meals for the day and type of meal insert
     * above
     */
    public Iterable<Meal> mealsByDateAndType() {
        return repository.mealsByDateAndType(date, mealType);
    }

    /**
     *
     * @param nDoses
     * @param meal
     *
     * Inserts the number of dishes cooked for one of the planned meals
     */
    public void insertQuantity(int nDoses, Meal meal) throws IllegalArgumentException {
        meal.cookedQuantity().changeCookedQuantity(nDoses);
    }

    /**
     *
     * @param selectedMeal
     * @throws DataConcurrencyException
     * @throws DataIntegrityViolationException
     * @return if the meal
     *
     * By confirming, it saves the information gathered in the database
     */
    public Meal save(Meal selectedMeal) throws DataConcurrencyException, DataIntegrityViolationException {
        return repository.save(selectedMeal);
    }
}