/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.BatchRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ismal
 */
public class SearchBatchController implements Controller {

    BatchRepository batchRepo = PersistenceContext.repositories().batches();
    MealRepository mealRepo = PersistenceContext.repositories().meals();

    /**
     * Go to BatchRepository and find a batch by its acronym
     *
     * @param acronym acronym
     * @return batch
     */
    public Batch findByAcronym(String acronym) {
        return batchRepo.findByAcronym(acronym);
    }

    /**
     * Compare batch of meals with select batch and add to a list meals with
     * that batches.
     *
     * @param batch Batch
     * @return list of meals
     */
    public Iterable<Meal> getMealsByBatch(Batch batch) {
        List<Meal> mealList = new ArrayList<>();

        for (Meal meal : mealRepo.findAll()) {
            if (meal.batches().contains(batch)) {
                mealList.add(meal);
            }
        }
        
        return mealList;
    }
    
}
