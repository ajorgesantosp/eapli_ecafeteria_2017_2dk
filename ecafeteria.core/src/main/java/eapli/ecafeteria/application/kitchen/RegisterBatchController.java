/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.domain.kitchen.BatchBuilder;
import eapli.ecafeteria.domain.kitchen.Material;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.BatchRepository;
import eapli.ecafeteria.persistence.MaterialRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author ismal
 */
public class RegisterBatchController implements Controller {

    BatchRepository batchRepo = PersistenceContext.repositories().batches();
    MaterialRepository materialRepo = PersistenceContext.repositories().materials();
    MealRepository mealsRepo = PersistenceContext.repositories().meals();

    public Iterable<Material> getAvaliableMaterials() {
        return materialRepo.findAll();
    }
    
    public Iterable<Meal> getAvaliableMeals() {
        return mealsRepo.findAll();
    }
    
    public Meal addBatchToMeal(Meal selectedMeal, Batch newBatch) throws DataConcurrencyException, DataIntegrityViolationException {
        selectedMeal.addBatch(newBatch);
        
        return mealsRepo.save(selectedMeal);
    }

    public Batch registerBatch(String description, String acronym, Material material, Integer quantity) throws DataConcurrencyException, DataIntegrityViolationException {

        BatchBuilder builder = new BatchBuilder();
        builder.withAcronym(acronym);
        builder.withDescription(description);
        builder.withIngredient(material);
        builder.withQuantity(quantity);

        Batch batch = builder.build();

        return batchRepo.save(batch);
    }

}
