/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.booking;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.CardMovements;
import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.CardMovementsRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author César Seabra Fábio Correia
 */
public class BookingMealCancelController implements Controller {

    private final ListBookingService svc = new ListBookingService();
    private final BookingRepository bookingRepo = PersistenceContext.repositories().bookings();
    private final CafeteriaUserRepository userRepository = PersistenceContext.repositories().cafeteriaUsers(null);
    private final CafeteriaUser currentUser = userRepository.findByUsername(Application.session().session().authenticatedUser().username());
    private Booking selectedBooking;
    private final CafeteriaUserRepository cafeteriaUser = PersistenceContext.repositories().cafeteriaUsers(null);
    private final CardMovementsRepository cardMovementsUser = PersistenceContext.repositories().cardMovements();

    /**
     *
     * @return
     */
    public Iterable<Booking> getReservedBookingsOfUser() {
        Calendar agora = Calendar.getInstance();
        return svc.bookingReservedOfUserThisDayOn(currentUser, agora);
    }

    /**
     *
     * @param refundTotalMoney
     * @return
     * @throws DataIntegrityViolationException
     * @throws DataConcurrencyException
     */
    public boolean changeBookingState(boolean refundTotalMoney) throws DataIntegrityViolationException, DataConcurrencyException {
        if (selectedBooking.changeState(Booking.States.Canceled)) {
            this.bookingRepo.save(selectedBooking);
            refundUserMealMoney(refundTotalMoney);
            return true;
        }
        return false;
    }

    /**
     *
     */
    private void refundUserMealMoney(boolean refundTotalMoney) throws DataConcurrencyException, DataIntegrityViolationException {
        double money;

        if (refundTotalMoney) {
            money = selectedBooking.meal().dish().currentPrice().amount();
        } else {
            money = selectedBooking.meal().dish().currentPrice().amount() / 2;
        }
        currentUser.cardAccount().setBalance(currentUser.cardAccount().getBalance() + money);
        CardMovements movement = new CardMovements();
        movement.setAmount(money);
        movement.setMecanographicNumber(currentUser.mecanographicNumber());
        movement.setDescription("Refund movement of booking nr: " + selectedBooking.id());
        cafeteriaUser.save(currentUser);
        cardMovementsUser.save(movement);
    }

    /**
     *
     * @param book
     * @return
     */
    public boolean validateHourToRefundMoney(Booking book) {
        Calendar currentTime = Calendar.getInstance();
        int currentHourSystem = currentTime.get(Calendar.HOUR_OF_DAY);
        this.selectedBooking = book;

        if (compareDates(currentTime, book.meal().date())) {
            
            if (selectedBooking.meal().mealType() == MealType.LUNCH) {
                if (currentHourSystem > 10) {
                    return false;
                }
            }

            if (selectedBooking.meal().mealType() == MealType.DINNER) {
                if (currentHourSystem > 16) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 
     * @param date1
     * @param date2
     * @return 
     */
    public boolean compareDates(Calendar date1, Calendar date2) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String d1 = df.format(date1.getTime());
        String d2 = df.format(date2.getTime());
        if (d1.equals(d2)) {
            return true;
        }
        return false;
    }

}
