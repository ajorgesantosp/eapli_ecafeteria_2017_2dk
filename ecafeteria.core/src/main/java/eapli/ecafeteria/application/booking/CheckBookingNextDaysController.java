/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.booking;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author nunom
 */
public class CheckBookingNextDaysController implements Controller {

    private final CafeteriaUserRepository userRepo = PersistenceContext.repositories().cafeteriaUsers(null);
    private final BookingRepository repository = PersistenceContext.repositories().bookings();
    private int nDays;

    private CafeteriaUser user;

    public CheckBookingNextDaysController() {
    }

    public boolean insertNumberOfDays(int numberDays) {
        if (numberDays >= 0) {
            this.nDays = numberDays;
            return true;
        }
        return false;
    }

    public List<Booking> getBookingNextDays() {
        getCurrentUser();
        Date currentDay = new Date();
        Date limitDay= new Date();
        limitDay.setDate(currentDay.getDate()+nDays);
        List<Booking> listBookingNextDays=(List)repository.getBookingForNextDays(user, currentDay, limitDay);
        return listBookingNextDays;
    }

    /**
     * Method that get the user
     *
     * @return user
     */
    private CafeteriaUser getCurrentUser() {
        user = userRepo.findByUsername(Application.session().session().authenticatedUser().username());

        return user;
    }

}