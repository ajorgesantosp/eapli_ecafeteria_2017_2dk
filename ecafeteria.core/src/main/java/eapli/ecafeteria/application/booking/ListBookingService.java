/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.booking;

import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.Calendar;

/**
 *
 * @author César Seabra
 */
public class ListBookingService {

    private final BookingRepository bookingRepo = PersistenceContext.repositories().bookings();

    public Iterable<Booking> bookingReservedOfUserThisDayOn(CafeteriaUser user, Calendar date) {
        return this.bookingRepo.bookingReservedOfUserThisDayOn(user, date);
    }

}
