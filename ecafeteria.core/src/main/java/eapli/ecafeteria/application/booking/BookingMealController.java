package eapli.ecafeteria.application.booking;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.booking.BookingBuilder;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.io.QRCode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;


public class BookingMealController implements Controller {

    private final BookingRepository bookingRepo = PersistenceContext.repositories().bookings();
    private final MealRepository mealRepo = PersistenceContext.repositories().meals();
    private final CafeteriaUserRepository userRepo = PersistenceContext.repositories().cafeteriaUsers(null);

    private CafeteriaUser user;

    /**
     * Method that get the user
     *
     * @return user
     */
    public CafeteriaUser getCurrentUser() {
        user = userRepo.findByUsername(Application.session().session().authenticatedUser().username());
        
        return user;
    }

    /**
     * Method that get the meals
     *
     * @param date Gregorian date
     * @param type Meal type
     * @return the meal of a certain date and a certain type
     */
    public Iterable<Meal> getMeals(Calendar date, MealType type) {
        return mealRepo.mealsByDateAndType(date, type);
    }
    
    /**
     * Checks if the meal selected by the user contains any of the allergens
     * in the user's profile
     * @param selectedMeal meal selected by the user
     * @return true if meal has, at least, one of the user's allergens
     */
    public boolean doesMealHaveUserAllergens(Meal selectedMeal){
        List<Allergen> userAllergens = user.profile().allergenList();
        List<Allergen> mealAllergens = selectedMeal.dish().allergenList();
        
        for (Allergen a : userAllergens) {
            if(mealAllergens.contains(a))
                return true;
        }
        
        return false;
    }

    /**
     * Method that save to the repository a meal
     *
     * @param meal meal
     * @return true if saved or false if user was null or didn't save
     * @throws eapli.framework.persistence.DataConcurrencyException
     * @throws eapli.framework.persistence.DataIntegrityViolationException user already booked the selected meal
     */
    public Booking bookMeal(Meal meal) throws DataConcurrencyException, DataIntegrityViolationException {

        BookingBuilder builder = new BookingBuilder();

        builder.withAccount(user);
        builder.withMeal(meal);

        Booking newBooking = builder.build();
        newBooking = bookingRepo.save(newBooking);

        // Checks if booking is saved succesfully in database
        if (newBooking != null) {
            // Updates the user's card account balance, locally and in the database
            user.cardAccount().subtractFromBalance(meal.dish().currentPrice().amount());
            
            // Checks if user's balance was updated in database successfully
            if (userRepo.save(user) == null) {
                bookingRepo.delete(newBooking);
                return null;
            }
            
            else
                creatQRCode(newBooking);
        }

        return newBooking;
    }

    /**
     * Method that create QRCode
     *
     * @param book book
     * @return true if QRCode was created or false if QRCode wasn't created
     */
    public boolean creatQRCode(Booking book) {

        String content;
        String idMeal = book.meal().id().toString();
         UUID uuid = UUID.randomUUID();
        String fileName = uuid.toString();
        String dishName = book.meal().dish().name().toString();
        String userName = user.id().toString();
        SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
        String date = format1.format(book.meal().date().getTime());
        content = dishName +" : "+ idMeal  + " : " + userName + " : " + date;
        BookingRepository bookings = PersistenceContext.repositories().bookings();

        return QRCode.createQRCode(content, fileName);
    }
    
    /**
     * Checks if user can afford the selected meal
     * @param selectedMeal meal selected by the user
     * @return true if user has enough balance to book meal. false otherwise
     */
    public boolean canUserAffordMeal(Meal selectedMeal)
    {
        double selectedMealPrice = selectedMeal.dish().currentPrice().amount();
        double userBalance = user.cardAccount().getBalance();
        
        return userBalance - selectedMealPrice >= 0;
    }
    
    public void setCurrentUser(CafeteriaUser user){
        this.user = user;
    }
}
