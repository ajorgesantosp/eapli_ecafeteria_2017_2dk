package eapli.ecafeteria.application.ratings;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.ratings.Comment;
import eapli.ecafeteria.domain.ratings.MealRating;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.CommentRepository;
import eapli.ecafeteria.persistence.MealRatingRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Date;
import eapli.util.DateTime;

/**
 *
 * @author 1150585 / 1150801
 */
public class RegisterMealRatingController implements Controller {

    private final MealRatingRepository ratingRepository = PersistenceContext.repositories().ratings();
    private final BookingRepository repositoryBooking = PersistenceContext.repositories().bookings();
    private final CafeteriaUserRepository userRepo = PersistenceContext.repositories().cafeteriaUsers(null);
    private final CommentRepository commentRepo = PersistenceContext.repositories().comments();

    private CafeteriaUser user;
    private MealRating newMealRating;
    private Comment newComment;

    /*
    * getUser is used to retrieve an user from the database providing a username with a valid session
     */
    public CafeteriaUser getUser() {
        return userRepo.findByUsername(Application.session().session().authenticatedUser().username());
    }

    public void setUser(CafeteriaUser user) {
        this.user = user;
    }

    /*
    * Verifies the booking of a logged user. User is known by calling the method setUser.
    * @param date - i.e 02/05/2017
    * @param typeMeal - i.e lunch
     */
    public boolean checkBooking(Date mealDate, MealType typeMeal) {

        Date today = new Date();

//        if (today.after(mealDate) || today.equals(mealDate)) {

            Booking b = repositoryBooking.checkIfBookingExists(user, Booking.States.Delivered, typeMeal, mealDate);

            if (b == null) {
                return false;
            } else {

                System.out.println(b.toString());
                return true;

            }

//        } else {
//            return false;
//        }

    }

    /*
    * Creates a new Instance of MealRating with the parameters provided by the user. 
    * Does not need to save to the database since it is already saved when a comment is attached.
    * 
    * @param date the date when the meal was consumed, e.g, 22/03/2017
    * @param typeMeal i.e Lunch, Dinner, Not Specified
    * @param rating number between 0 and 5
     */
    public MealRating registerNewRatingWithComment(Date mealDate, MealType typeMeal, int rating)
            throws DataConcurrencyException, DataIntegrityViolationException {

        newMealRating = new MealRating(user, mealDate, typeMeal, rating);
        return newMealRating;
    }

    /* 
    * Creates a new Instance of MealRating and saves it to the database when no comment is provided.
    *
    * @param date the date when the meal was consumed, e.g, 22/03/2017
    * @param typeMeal i.e Lunch, Dinner, Not Specified
    * @param rating number between 0 and 5
     */
    public MealRating registerNewRatingWithoutComment(Date mealDate, MealType typeMeal, int rating)
            throws DataConcurrencyException, DataIntegrityViolationException {
        newMealRating = new MealRating(user, mealDate, typeMeal, rating);
        return this.ratingRepository.save(newMealRating);
    }

    /*
    * Creates a new instance of Comment with the comment provided by a user
    * @param comment the comment provided by the user
     */
    public Comment createComment(String comment)
            throws DataConcurrencyException, DataIntegrityViolationException {

        newComment = new Comment(comment, newMealRating);
        return this.commentRepo.save(newComment);
    }

}
