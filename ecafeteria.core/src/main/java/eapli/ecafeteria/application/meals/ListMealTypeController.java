/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;

/**
 *
 * Created by 1150450 and 1150444 on 04/05/2017.
 */
public class ListMealTypeController implements Controller {
    
     private ListMealTypeService svc = new ListMealTypeService();

    public Iterable<MealType> allMealTypes() {
        return svc.allMealTypes();
    }
    
}
