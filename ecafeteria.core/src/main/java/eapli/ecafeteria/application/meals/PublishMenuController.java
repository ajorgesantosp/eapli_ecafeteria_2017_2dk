/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Leandro Santos
 */
public class PublishMenuController implements Controller {

    private final MenuRepository menuRepository = PersistenceContext.repositories().menus();
    private Menu  menu;
    public Iterable<Menu> listMenusInProgress() {
        
        return menuRepository.findMenuInProgress();
    }

     public void chooseMenu(Menu menu) {
        this.menu = menu;
    }
    public Menu publishMenu(Menu menu) throws DataIntegrityViolationException, DataConcurrencyException, NullPointerException {
        if (!menu.menuPublished()) {
            return null;
        }

        return menuRepository.save(menu);
    }

}
