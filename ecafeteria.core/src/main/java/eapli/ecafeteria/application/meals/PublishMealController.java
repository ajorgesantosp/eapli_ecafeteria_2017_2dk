/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealBuilder;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.BatchRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.List;

/**
 *
 * Created by 1150450 and 1150444 on 02/05/2017.
 */
public class PublishMealController implements Controller {

    private final DishRepository dishRepository = PersistenceContext.repositories().dishes();
    private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    private final BatchRepository batchRepository = PersistenceContext.repositories().batches();

    public PublishMealController() {
    }

    public Meal buildMeal(MealType mealType, Dish dish, String dateString) {
        Calendar dateCalendar = DateTime.parseDate(dateString);

        if (dateCalendar == null) {
            return null;
        }

        final MealBuilder builder = new MealBuilder();

        builder.withMealType(mealType).withDish(dish).withDate(dateCalendar);

        return builder.build();
    }

    public Meal publishMeal(Meal newMeal) throws DataIntegrityViolationException, DataConcurrencyException {

        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        List<Meal> duplicateMealList = (List<Meal>) mealRepository.duplicateMeals(newMeal);
        if (!duplicateMealList.isEmpty()) {
            return null;
        }

        return mealRepository.save(newMeal);
    }

    public Iterable<MealType> allMealTypes() {
        return MealType.mealTypes();

    }

    public Iterable<Dish> allActiveDishes() {
        return dishRepository.allActiveDishes();
    }

    public Iterable<Batch> allBatches() {
        return batchRepository.findAll();
    }

}
