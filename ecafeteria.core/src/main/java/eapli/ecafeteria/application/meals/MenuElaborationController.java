/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Leandro Santos
 */
public class MenuElaborationController implements Controller {

    private final MenuRepository menuRepository = PersistenceContext.repositories().menus();
    private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    private Menu menu;

    public Menu elaborateMenu(Calendar startDate, Calendar finishDate) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        menu = new Menu(startDate, finishDate);

        if (menuRepository.findByStartDate(startDate) != null || menuRepository.findByFinishDate(finishDate) != null) {
            return null;
        }

        return menu;
    }

    public void chooseMenu(Menu menu) {
        this.menu = menu;
    }

    public Iterable<Menu> menus() {
        return menuRepository.findMenuInProgress();
    }

    public List<Meal> availableMeals() {
        List<Meal> availableMeals = new ArrayList<>();
        for (Meal m : mealRepository.allMealsDateInterval(menu.startDate(), menu.finishDate())) {
            if (!menu.mealList().contains(m)) {
                availableMeals.add(m);
            }
        }

        return availableMeals;
    }

    public boolean addMeal(Meal meal) {
        return menu.addMeal(meal);
    }

    public boolean removeMeal(Meal meal) {
        return menu.removeMeal(meal);
    }

    public List<Meal> menuMealList(){
        return menu.mealList();
    }

    public Calendar parseDate(String dateString) {
        return DateTime.parseDate(dateString);
    }

    public Menu saveMenu() throws DataIntegrityViolationException, DataConcurrencyException {
        return menuRepository.save(menu);
    }

}
