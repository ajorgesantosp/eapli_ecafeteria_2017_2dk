/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cashier;

import eapli.ecafeteria.domain.delivery.Cashier;
import eapli.ecafeteria.domain.delivery.CashierState;
import eapli.ecafeteria.domain.delivery.Shift;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CashierRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author 1150450 and 1150585
 */
public class ViewAvailableQuantsMealController {

    private final CashierRepository cashierRepository = PersistenceContext.repositories().cashier();

    private final Cashier currentCashier = cashierRepository.findById(1L);

    public ViewAvailableQuantsMealController() {
    }

    public CashierState cashierState() {
        return currentCashier.state();
    }

    public Map<Dish, Integer> numAvailableQuantsMeal() {

        Shift currentShift = currentCashier.currentShift();

        Map<Dish, Integer> dishCookedQuant = new HashMap<>();
        for (Meal meal : currentShift.mealList()) {
            dishCookedQuant.put(meal.dish(), meal.cookedQuantity().cookedQuantity());
        }

        return dishCookedQuant;
    }

}
