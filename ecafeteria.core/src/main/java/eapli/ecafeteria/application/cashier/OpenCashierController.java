package eapli.ecafeteria.application.cashier;

import eapli.ecafeteria.domain.delivery.Cashier;
import eapli.ecafeteria.domain.delivery.CashierState;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.CashierRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author 1150416
 */
public class OpenCashierController implements Controller{
    /**
     * The repository of cashier
     */
    private final CashierRepository cashierRepositoryUnit = PersistenceContext.repositories().cashier();
    
    /**
     * The meals repository
     */
    private final MealRepository mealRepoUnit = PersistenceContext.repositories().meals();
        
    /**
     * The cashier to be closes
     */
    private Cashier cashier;
    
    /**
     * Method that checks the state of the cashier and returns the types of meal
     */
    public void startOpenCashierOperation(){
        cashier = cashierRepositoryUnit.findById(new Long(1));
        
        if(cashier.checkState(CashierState.OPEN)){
            throw new IllegalStateException("The cashier is already opened.");
        }
    }
    
    /**
     * Opens the cashier shift and changes it's state to open
     * @param date the date of the shift to open
     * @param hour the hour of the shift to open
     * @return true if the cashier was successfully opened, false otherwise
     * @throws DataConcurrencyException
     * @throws DataIntegrityViolationException 
     */
    public boolean openCashierShift(Calendar date, Calendar hour) throws DataConcurrencyException, DataIntegrityViolationException{
        MealType mealType = MealType.mealTypeByHour(hour);
        date.setTimeInMillis(date.getTimeInMillis() + hour.getTimeInMillis() + 3600000L);
        ArrayList<Meal> lstMeal = (ArrayList<Meal>) mealRepoUnit.mealsByDateAndType(date, mealType);
        if(cashier.openCashier(date, lstMeal)){
            cashierRepositoryUnit.save(cashier);
            return true;
        }
            return false;
    }
}
