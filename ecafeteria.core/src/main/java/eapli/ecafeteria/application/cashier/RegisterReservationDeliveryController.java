/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cashier;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.delivery.Cashier;
import eapli.ecafeteria.domain.delivery.CashierState;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CashierRepository;

import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Pattern;

/**
 *
 * @author fabio e cesar
 */
public class RegisterReservationDeliveryController implements Controller {

    private Booking currentReservation;
    private String QRCodeInfo = "";
    private final BookingRepository bookingsRepository = PersistenceContext.repositories().bookings();
    private final CashierRepository cashierRepositoryUnit = PersistenceContext.repositories().cashier();

    /**
     * Search in repository if exists an reservation with the code inserted.
     *
     * @param informationQRcode
     * @return true or false if the reservation is found or not
     */
    public boolean searchUserReservation(String informationQRcode) {

        informationQRcode = informationQRcode.replaceAll(" ", "");
        String[] info = informationQRcode.split(Pattern.quote(":"));
        String idMEal = info[1];
        String userName = info[2];
        MecanographicNumber mec = new MecanographicNumber(userName);
        CafeteriaUser userCaf = PersistenceContext.repositories().cafeteriaUsers(null).findByMecanographicNumber(mec);
        Meal m = PersistenceContext.repositories().meals().findByID(Long.parseLong(idMEal));

        this.currentReservation = bookingsRepository.reserveByUserStateMealID(userCaf, Booking.States.Reserved, m);

        if (currentReservation == null) {
            return false;
        }

        return true;

    }

    /**
     * Verifies if the booking matches the current shift MealType and has the
     * same date as the current one.
     *
     * @return true if it meets the conditions or false if it doesn't
     */
    public boolean verifyBookingIntegrity() {
        Calendar now = Calendar.getInstance();
        MealType mT = MealType.mealTypeByHour(now);

        if (currentReservation.meal().mealType() == mT) {
            if (compareDates(now, currentReservation.meal().date())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Uses the Qrcode Reader class to read an image containing the QRCode of
     * the reservation to retrieve the code of the reservation.
     *
     * @param filePath name of the QRCode image
     * @return the info present in the QRCode
     * @throws java.io.IOException
     */
    public String getReservationInfoFromQRCodeTag(String filePath) throws IOException {
        QRCodeInfo = eapli.util.io.QRCode.readFromFile(filePath);
        return QRCodeInfo;
    }

    /**
     * Change the state of a delivery reservation.
     *
     * @return true or false if the changing of the reservation status is made
     * with success or not
     * @throws eapli.framework.persistence.DataIntegrityViolationException
     * @throws eapli.framework.persistence.DataConcurrencyException
     */
    public boolean changeReservationState() throws DataIntegrityViolationException, DataConcurrencyException {

        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_DELIVERY);

        if (currentReservation == null) {
            throw new IllegalArgumentException();

        }

        if (currentReservation.changeState(Booking.States.Delivered)) {
            Booking ret = this.bookingsRepository.save(currentReservation);
            return true;
        }
        return false;
    }

    /**
     * Verifies if the cashier is open or not. If the cashier istn't open it
     * will throw a IllegalStateException exception.
     */
    public void verifyStateofCashier() {
        Cashier cashier = cashierRepositoryUnit.findById(new Long("1"));
        if (cashier.checkState(CashierState.CLOSED)) {
            throw new IllegalStateException("You must open the cashier first!");
        }
    }

    /**
     * Returns the informartion of the reservation. This method is used to
     * gather the info and show it to the cashier user.
     *
     * @return info of the reservation
     */
    public String getBookingData() {
        String data;
        Designation dishName = currentReservation.meal().dish().name();
        Calendar mealDate = currentReservation.meal().date();
        Long mealID = currentReservation.id();
        String mealType = currentReservation.meal().mealType().toString();

        data = "Current reservation ID: " + mealID + "\n"
                + "Dish name: " + dishName.toString() + "\n"
                + "Meal date: " + DateTime.format(mealDate) + "\n"
                + "Meal type: " + mealType + "\n";

        return data;
    }

    /**
     * Compares the dates to verify two dates.
     *
     * @param date1 date of the booking
     * @param date2 date of the current day
     * @return true if they are equal, false otherwise
     */
    public static boolean compareDates(Calendar date1, Calendar date2) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String d1 = df.format(date1.getTime());
        String d2 = df.format(date2.getTime());
        return d1.equals(d2);
    }
    
}
