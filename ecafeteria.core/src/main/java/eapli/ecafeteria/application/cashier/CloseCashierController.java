package eapli.ecafeteria.application.cashier;

import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.delivery.Cashier;
import eapli.ecafeteria.domain.delivery.CashierState;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CashierRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;

/**
 * 
 * @author 1150416
 */
public class CloseCashierController implements Controller{
    /**
     * The repository of cashier
     */
    private final CashierRepository cashierRepositoryUnit = PersistenceContext.repositories().cashier();
    
    /**
     * The repository of meal plan items
     */
    private final BookingRepository bookingRepositoryUnit = PersistenceContext.repositories().bookings();
    
    /**
     * The cashier to be closes
     */
    private Cashier cashier;
  
    /**
     * The list of all bookings
     */
    private ArrayList<Booking> bookings;
    /**
     * Starts the close cashier operation. Gets all the info needed in next steps.
     * @return list of the meals names
     */
    public ArrayList<String> startCloseCashierOperation() {
        cashier = cashierRepositoryUnit.findById(new Long("1"));
        
        if(cashier.checkState(CashierState.CLOSED)){
            throw new IllegalStateException("Cashier is already Close");
        }
        
        bookings = (ArrayList<Booking>) bookingRepositoryUnit.activeReserves();

        return (ArrayList<String>) cashier.nameOfMealsServed();

    }

    /**
     * Method to count the meals that were booked, but not delivered
     * @return the number of meals that weren't delivered meals
     * @throws eapli.framework.persistence.DataConcurrencyException
     * @throws eapli.framework.persistence.DataIntegrityViolationException
     */
    public ArrayList<Integer> countMeals() throws DataConcurrencyException, DataIntegrityViolationException {     
        return (ArrayList<Integer>) cashier.countBookingDiffOfMeals();
    }

    /**
     * Method that calls functions that execute the logout of the cashier operator
     * @return true if succeeded, false if failed
     * @throws eapli.framework.persistence.DataConcurrencyException
     * @throws eapli.framework.persistence.DataIntegrityViolationException
     */
    public boolean executeLogout() throws DataConcurrencyException, DataIntegrityViolationException {
        cashier.closeCashier();
        
        cashierRepositoryUnit.save(cashier);
        
        return true;
    }
    
}
