/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cashier;

import eapli.ecafeteria.domain.authz.CardAccount;
import eapli.ecafeteria.domain.authz.CardMovements;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.delivery.Cashier;
import eapli.ecafeteria.domain.ratings.Comment;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.CardMovementsRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import javax.persistence.NoResultException;

/**
 *
 * @author ASUS
 */
public class ReloadCardController {

  private final CafeteriaUserRepository cafeteriaUser =PersistenceContext.repositories().cafeteriaUsers(null);
  private final CardMovementsRepository cardMovementsUser =PersistenceContext.repositories().cardMovements();
  
  /**MecanograpgicNumber of user */
  
  
  
    private MecanographicNumber mecanographicNumber;
     /**Cafeteria user that is going to recharge is CardAcoount*/
    private CafeteriaUser user;
    private Cashier cashierUser;
    private double amount;
    private CardMovements movement;
           

    public ReloadCardController() { 
        this.cashierUser=new Cashier();
        
        
    }
    
    
    
    
    
    public void getUserByNumber(MecanographicNumber mecanographicNumber){
       this.mecanographicNumber=mecanographicNumber;
        user=cafeteriaUser.findByMecanographicNumber(mecanographicNumber);
        if(user==null){
            throw new NoResultException();
        }        
       
        
    }
    
    
    
    
    public String generateMBReference(double amount) throws Cashier.NegativeAmountException {
       this.amount=amount;
        cashierUser.generateMBreference(amount);
        
        return cashierUser.generateMBreference(amount);
        
        
        
    }
    
    
    public void confirmPayment() throws DataConcurrencyException, DataIntegrityViolationException, CardMovements.NegativeAmountException{
        
        cashierUser.confirmPayment(user,amount );
       movement=new CardMovements(mecanographicNumber,amount,"Credit movemnt!");
       user.cardAccount().addMovement(movement);
       
    }
    
    
    public CafeteriaUser getUser() {
        return user;
    }

    public static class NegativeAmountException extends Exception {

        public NegativeAmountException() {
            System.out.println("The amount must be a positive number and bigger than 0");
        }
    }
    
    
    public void saveUser () throws DataConcurrencyException, DataIntegrityViolationException{
        cafeteriaUser.save(user);
    }
    
    public void saveMovement () throws DataConcurrencyException, DataIntegrityViolationException{
        cardMovementsUser.save(movement);
    }
    
   
}