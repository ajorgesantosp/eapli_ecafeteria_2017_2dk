/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.authz;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.List;

/**
 *
 * @author Nuno
 */
public class EditProfileAllergensController implements Controller {

    private final CafeteriaUserRepository repository;
    CafeteriaUser loggedUser;

    public EditProfileAllergensController() {
        repository = PersistenceContext.repositories().cafeteriaUsers(null);
    }

    /**
     * Finds the logged in cafeteria user.
     *
     * @return
     */
    public void loggedUser() {
        loggedUser = this.repository.findByUsername(Application.session().session().authenticatedUser().username());
    }

    public Iterable<Allergen> allAllergenList() {
        return Allergen.allergens();
    }

    public List<Allergen> profileAllergenList() {
        return loggedUser.profile().allergenList();
    }

    public boolean addAllergen(Allergen selectedAllergen) {
        return loggedUser.profile().addAllergen(selectedAllergen);
    }

    public boolean removeAllergen(Allergen selectedAllergen) {
        return loggedUser.profile().removeAllergen(selectedAllergen);
    }

    public CafeteriaUser save() throws DataConcurrencyException, DataIntegrityViolationException {
        return repository.save(loggedUser);
    }
}
