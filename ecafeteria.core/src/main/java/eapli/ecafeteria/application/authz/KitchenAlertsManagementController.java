/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.authz;

import eapli.ecafeteria.domain.kitchen.KitchenAlertManagement;
import eapli.ecafeteria.persistence.AlertRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author 1150425 & 1150482
 */
public class KitchenAlertsManagementController {

    private final AlertRepository repository = PersistenceContext.repositories().AlertRepository();
    private final String name = "Kitchen Alert";
    private final KitchenAlertManagement am;

    public KitchenAlertsManagementController() {
        am = new KitchenAlertManagement(this.name);
        am.changeYellowAlert(this.repository.findByName(this.name).yellowAlertValue());
        am.changeRedAlert(this.repository.findByName(this.name).redAlertValue());
    }

    public boolean changeYellowAlert(int value) throws DataConcurrencyException, DataIntegrityViolationException {

        am.changeYellowAlert(value);
        this.repository.save(am);
        return true;

    }

    public boolean changeRedAlert(int value) throws DataConcurrencyException, DataIntegrityViolationException {

        am.changeRedAlert(value);
        this.repository.save(am);

        return true;
    }

    public boolean changeBothAlert(int yellowValue, int redValue) throws DataConcurrencyException, DataIntegrityViolationException {

        am.changeRedAlert(redValue);
        am.changeRedAlert(yellowValue);
        this.repository.save(am);
        return true;

    }

    public String alertValuesMessage() {
        return this.am.alertValues();
    }
}
