package eapli.ecafeteria.application.authz;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.framework.application.Controller;
import static javafx.application.Platform.exit;
import org.eclipse.persistence.sessions.Session;

/**
 *
 * @author 1150416
 */
public class LogoutController implements Controller{            
    public boolean logout(){
        try{
          
            Application.session().setSession(null);
           
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
