/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.authz;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Nuno
 */
public class EditProfileSaltPerWeekController implements Controller {

    private final CafeteriaUserRepository repository = PersistenceContext.repositories().cafeteriaUsers(null);

    public EditProfileSaltPerWeekController() {
    }

    /**
     * Finds the logged in cafeteria user.
     *
     * @return
     */
    private CafeteriaUser loggedUser() {
        return this.repository.findByUsername(Application.session().session().authenticatedUser().username());
    }

    public CafeteriaUser changeSaltQuantWeek(Integer saltQuantWeek) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);

        CafeteriaUser user = loggedUser();

        user.profile().changeSaltQuantWeek(saltQuantWeek);
        return repository.save(user);
    }
}
