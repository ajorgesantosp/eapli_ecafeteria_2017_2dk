/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author Esquilo
 */
public class BalanceController {
    private final CafeteriaUserRepository cafeteriaUser =PersistenceContext.repositories().cafeteriaUsers(null);
 
    private CafeteriaUser user;

    public BalanceController() {
    }
    
     public void getUser(){
       
        user=cafeteriaUser.findByUsername(Application.session().session().authenticatedUser().username());
       
    }
      
      public double getBalance(){
          return user.cardAccount().getBalance();
          
          
      }
}
