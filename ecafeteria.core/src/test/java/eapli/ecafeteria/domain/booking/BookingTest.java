/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.booking;

import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.booking.Booking.States;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUserBuilder;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealBuilder;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.NutricionalInfo;
import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import eapli.util.DateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ismal
 */
public class BookingTest {

    private DishType carne;
    private NutricionalInfo aNutricionalInfo;
    private final Designation prego = Designation.valueOf("Prego");
    private final Allergen celery = Allergen.CELERY;

    public BookingTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        carne = new DishType("carne", "carne");
        aNutricionalInfo = new NutricionalInfo(10, 11);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of changeState method, of class Booking.
     */
    @Test
    public void testChangeState() {

        final Set<RoleType> roles = new HashSet<>();
        roles.add(RoleType.ADMIN);

        Date date = new Date(); // current date

        final Meal aMeal = new MealBuilder()
                .withDate(DateTime.dateToCalendar(date))
                .withDish(new Dish(carne, prego, aNutricionalInfo, Money.euros(8)))
                .withMealType(MealType.LUNCH)
                .build();

        final CafeteriaUser aCafeteriaUser = new CafeteriaUserBuilder()
                .withOrganicUnit(new OrganicUnit("dummy", "dummy", "dummy")).withMecanographicNumber("DUMMY")
                .withSystemUser(new SystemUser("dummy", "duMMy1", "dummy", "dummy", "a@b.ro", roles)).build();

        final Booking aBooking = new BookingBuilder()
                .withAccount(aCafeteriaUser)
                .withMeal(aMeal)
                .build();

        System.out.println("changeState");
        Booking.States newState = States.Reserved;
        Booking instance = aBooking;
        boolean expResult = true;
        boolean result = instance.changeState(newState);
        assertEquals(expResult, result);
    }

    /**
     * Test of sameAs method, of class Booking.
     */
    @Test
    public void testSameAs() {
        final Set<RoleType> roles = new HashSet<>();
        roles.add(RoleType.ADMIN);

        Date date = new Date(); // current date

        final Meal aMeal = new MealBuilder()
                .withDate(DateTime.dateToCalendar(date))
                .withDish(new Dish(carne, prego, aNutricionalInfo, Money.euros(8)))
                .withMealType(MealType.LUNCH)
                .build();

        final CafeteriaUser aCafeteriaUser = new CafeteriaUserBuilder()
                .withOrganicUnit(new OrganicUnit("dummy", "dummy", "dummy")).withMecanographicNumber("DUMMY")
                .withSystemUser(new SystemUser("dummy", "duMMy1", "dummy", "dummy", "a@b.ro", roles)).build();

        final Booking aBooking = new BookingBuilder()
                .withAccount(aCafeteriaUser)
                .withMeal(aMeal)
                .build();

        final Booking aBooking2 = new BookingBuilder()
                .withAccount(aCafeteriaUser)
                .withMeal(aMeal)
                .build();

        System.out.println("sameAs");
        Object obj = aBooking2;
        Booking instance = aBooking;
        boolean expResult = true;
        boolean result = instance.sameAs(obj);
        assertEquals(expResult, result);
    }

}
