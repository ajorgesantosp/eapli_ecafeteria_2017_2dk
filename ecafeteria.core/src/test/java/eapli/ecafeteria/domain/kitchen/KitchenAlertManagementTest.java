/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author 1150425
 */
public class KitchenAlertManagementTest {
    
    public KitchenAlertManagement am;
    
    public KitchenAlertManagementTest() {
        this.am = new KitchenAlertManagement("Kitchen Alert", 50, 75);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     @Test
    public void testYellowValuesCannotBeTheSame(){
        am.changeYellowAlert(50);
    }
    
    @Test
     public void testRedValuesCannotBeTheSame(){
        am.changeRedAlert(75);
    }
    
     @Test
     public void testYellowValueCannotBeSuperiorToRedValue(){
         am.changeYellowAlert(76);
     }
     
     @Test
      public void testRedValueCannotBeInferiorToYellowValue(){
         am.changeYellowAlert(76);
     }
      
      @Test
      public void testYellowValueCannotBeInferiorTo0(){
          am.changeYellowAlert(-1);
      }
      
      @Test
      public void testYellowValueCannotBeSuperiorTo100(){
          am.changeYellowAlert(101);
      }
      
      @Test
      public void testRedValueCannotBeInferiorTo0(){
          am.changeRedAlert(-1);
      }
      
      @Test
      public void testRedValueCannotBeSuperiorTo100(){
          am.changeRedAlert(101);
      }
    
}
