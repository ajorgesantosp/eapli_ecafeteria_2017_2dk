/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.authz;

import eapli.ecafeteria.domain.meals.Allergen;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Leandro Santos
 */
public class ProfileTest {

    Profile instance;

    public ProfileTest() {
    }

    @Before
    public void setUp() {
        instance = new Profile(10, 20, 30);
        instance.addAllergen(Allergen.GLUTEN);
        instance.addAllergen(Allergen.EGGS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSaltQuantWeekMustNotBeNegative() {
        System.out.println("saltQuantWeek must not be minor than 0.");
        Profile testInstance = new Profile(-1, 0, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalQuantMealMustNotBeNegative() {
        System.out.println("calQuantMeal must not be minor than 0.");
        Profile testInstance = new Profile(0, -1, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalQuantWeekMustNotBeNegative() {
        System.out.println("calQuantWeek must not be minor than 0.");
        Profile testInstance = new Profile(0, 0, -1);
    }

    /**
     * Test of changeSaltQuantWeek method, of class Profile.
     */
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeSaltQuantWeekToNegative() {
        System.out.println("changeSaltQuantWeek - new saltQuantWeek must not be minor than 0.");
        instance.changeSaltQuantWeek(-1);
    }

    /**
     * Test of changeCalQuantMeal method, of class Profile.
     */
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeCalQuantMealToNegative() {
        System.out.println("changeCalQuantMeal - new calQuantMeal must not be minor than 0.");
        instance.changeCalQuantMeal(-1);
    }

    /**
     * Test of changeCalQuantWeek method, of class Profile.
     */
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeCalQuantWeekToNegative() {
        System.out.println("changeCalQuantWeek - new calQuantWeek must not be minor than 0.");
        instance.changeCalQuantWeek(-1);
    }

    @Test(expected = IllegalStateException.class)
    public void testAddAllergenCannotBeNull() {
        System.out.println("Added allergen mustn't be null.");
        instance.addAllergen(null);
    }

    @Test(expected = IllegalStateException.class)
    public void testRemoveAllergenCannotBeNull() {
        System.out.println("Removed allergen mustn't be null.");
        instance.removeAllergen(null);
    }

    @Test
    public void testAddAllergenCannotBeDuplicate() {
        System.out.println("Cannot have the same allergen twice.");
        boolean expectedResult = false;
        boolean result = instance.addAllergen(Allergen.EGGS);
        assertEquals(expectedResult, result);
    }

    @Test
    public void testRemoveAllergenMustExist() {
        System.out.println("The allergen beeing removed must exist.");
        boolean expectedResult = false;
        boolean result = instance.removeAllergen(Allergen.SULPHURDIOXIDESULPHITES);
        assertEquals(expectedResult, result);
    }
}
