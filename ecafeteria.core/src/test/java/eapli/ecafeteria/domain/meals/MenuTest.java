/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author diogo
 */
public class MenuTest {

    private Menu instance;
    private Menu instance1;
    private Dish dish;
    private MealType mealType;
    private Calendar startDate;
    private Calendar finishDate;
    private Meal meal;
    private int expectedQuantity = 100;

    public MenuTest() {
    }

    @Before
    public void setUp() {
        Designation name = Designation.valueOf("Bolonhesa");

        dish = new Dish(new DishType("Carne", "Carne"), name,
                new NutricionalInfo(10, 11),
                new Money(3.60, Currency.getInstance("EUR")));

        mealType = MealType.LUNCH;

        startDate = DateTime.parseDate("23-12-2017");

        finishDate = DateTime.parseDate("28-12-2017");

        instance = new Menu(startDate, finishDate);

        instance1 = new Menu(startDate, finishDate);

        dish = new Dish(new DishType("Carne", "Carne"), name,
                new NutricionalInfo(10, 11),
                new Money(3.60, Currency.getInstance("EUR")));

        Calendar date = DateTime.parseDate("23-12-2017");

        meal = new Meal(MealType.LUNCH, dish, date, expectedQuantity);

        instance.addMeal(meal);
        instance.isInProgress();
    }

    /**
     * Test of startDate method, of class Menu.
     */
    @Test
    public void testStartDate() {
        System.out.println("startDate");
        setUp();
        Calendar expResult = DateTime.parseDate("23-12-2017");
        Calendar result = instance.startDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of finishDate method, of class Menu.
     */
    @Test
    public void testFinishDate() {
        System.out.println("finishDate");
        setUp();
        Calendar expResult = DateTime.parseDate("28-12-2017");
        Calendar result = instance.finishDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of mealList method, of class Menu.
     */
    @Test
    public void testMealList() {
        System.out.println("mealList");
        setUp();
        instance1.addMeal(meal);
        List<Meal> expResult = instance1.mealList();
        List<Meal> result = instance.mealList();
        assertEquals(expResult, result);
    }

    /**
     * Test of addMeal method, of class Menu.
     */
    @Test
    public void testAddMeal() {
        System.out.println("addMeal");
        setUp();
        Calendar date = DateTime.parseDate("23-12-2017");
        Meal meal1 = new Meal(MealType.DINNER, dish, date, expectedQuantity);
        boolean expResult = true;
        boolean result = instance.addMeal(meal1);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeMeal method, of class Menu.
     */
    @Test
    public void testRemoveMeal() {
        System.out.println("removeMeal");
        setUp();
        Calendar date = DateTime.parseDate("23-12-2017");
        Meal meal1 = new Meal(MealType.DINNER, dish, date,expectedQuantity);
        boolean expResult = false;
        boolean result = instance.removeMeal(meal1);
        assertEquals(expResult, result);
    }

//    /**
//     * Test of containsMeal method, of class Menu.
//     */
//    @Test
//    public void testContainsMeal() {
//        System.out.println("containsMeal");
//        setUp();
//        boolean expResult = true;
//        Calendar date = DateTime.parseDate("23-12-2017");
//        Meal meal = new Meal(MealType.LUNCH, dish, date, expectedQuantity);
//
//        boolean result = instance.containsMeal(meal);
//        assertEquals(expResult, result);
//    }

    /**
     * Test of menuPublished method, of class Menu.
     */
    @Test
    public void testMenuPublished() {
        System.out.println("menuPublished");
        setUp();
        boolean expResult = true;
        boolean result = instance.menuPublished();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInProgress method, of class Menu.
     */
    @Test
    public void testIsInProgress() {
        System.out.println("isInProgress");
        setUp();
        boolean expResult = true;
        boolean result = instance.isInProgress();
        assertEquals(expResult, result);
    }

}
