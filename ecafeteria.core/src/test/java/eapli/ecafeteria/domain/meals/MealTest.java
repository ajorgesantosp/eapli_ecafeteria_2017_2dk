/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.domain.kitchen.Material;
import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1150444 & 1150450
 */
public class MealTest {

    private Dish dish;
    private MealType mealType;
    private Calendar date;
    private List<Batch> batches;
    private Menu menu;

    public MealTest() {
    }

    @Before
    public void setUp() {
        Designation name = Designation.valueOf("Bolonhesa");
        dish = new Dish(new DishType("Carne", "Carne"), name,
                new NutricionalInfo(10, 11),
                new Money(3.60, Currency.getInstance("EUR")));
        mealType = MealType.LUNCH;
        date = DateTime.parseDate("23-12-2017");

        Batch batch1 = new Batch(new Material("DUMMY", "DUMMY"), "DUMMY", "DUMMY", 1);
        Batch batch2 = new Batch(new Material("DUMMY", "DUMMY"), "DUMMY", "DUMMY", 2);
        batches = new ArrayList<>();
        batches.add(batch1);
        batches.add(batch2);
    }

//    @Test(expected = IllegalStateException.class)
//    public void testDishMustNotBeNull() {
//        System.out.println("must have a Dish");
//        //Meal instance = new Meal(mealType, null, date, batches,menu);
//    }
//
//    @Test(expected = IllegalStateException.class)
//    public void testMealTypeMustNotBeNull() {
//        System.out.println("must have a MealType");
//        //Meal instance = new Meal(null, dish, date, batches,menu);
//    }

//    @Test(expected = IllegalStateException.class)
//    public void testDateMustNotBeNull() {
//        System.out.println("must have a Date");
//        Meal instance = new Meal(mealType, dish, null, batches,menu);
//    }

//    @Test(expected = IllegalStateException.class)
//    public void testBatchesMustNotBeNull() {
//        System.out.println("must have Batches");
//        Meal instance = new Meal(mealType, dish, date, null);
//    }

}
