/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.util.DateTime;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nunom
 */
public class MealTypeTest {
    
    public MealTypeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }



    /**
     * Test of mealType method, of class MealType.
     */
    @Test
    public void testMealType() {
        System.out.println("mealType");
        String mealType = "lunch";
        
        MealType result = MealType.mealType(mealType.toUpperCase());
        assertTrue(MealType.LUNCH.equals(result));
                

        MealType result2 = MealType.mealType(mealType.toUpperCase());
        assertFalse(MealType.DINNER.equals(result2));
    }

    /**
     * Test of dateMealType method, of class MealType.
     */
    @Test
    public void testDateMealType() {
        System.out.println("dateMealType");
        Date expResult = new Date();
        expResult.setTime(12);
        Date result = MealType.LUNCH.dateMealType();
        assertEquals(expResult, result);
        expResult.setTime(10);
        Date result2 = MealType.LUNCH.dateMealType();
        assertFalse(expResult.equals(result2));
    }

    /**
     * Test of mealTypeByHour method, of class MealType.
     */
    @Test
    public void testMealTypeByHourToLunch() {
        System.out.println("mealTypeByHour");
        Calendar hourCalendar = DateTime.parseDate("14:00","HH:mm");
        MealType expResult = MealType.LUNCH;
        MealType result = MealType.mealTypeByHour(hourCalendar);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of mealTypeByHour method, of class MealType.
     */
    @Test
    public void testMealTypeByHourToDinner() {
        System.out.println("mealTypeByHour");
        Calendar hourCalendar = DateTime.parseDate("19:00","HH:mm");
        MealType expResult = MealType.DINNER;
        MealType result = MealType.mealTypeByHour(hourCalendar);
        assertEquals(expResult, result);
    }
}
