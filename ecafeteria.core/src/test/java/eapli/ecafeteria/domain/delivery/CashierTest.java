/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.delivery;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author nunom
 */
public class CashierTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
 
    @Before
    public void setUp() {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of openCashier method, of class Cashier.
     */
    @Test
    public void testOpenCashier() {
        System.out.println("openCashier");
        Calendar date = DateTime.now();
        List<Meal> lstMeal = null;
        Cashier instance = new Cashier();
        boolean expResult = true;
        boolean result = instance.openCashier(date, lstMeal);
        assertEquals(expResult, result);
    }

    /**
     * Test of state method, of class Cashier.
     */
    @Test
    public void testState() {
        System.out.println("state");
        Cashier instance = new Cashier();
        CashierState expResult = CashierState.CLOSED;
        CashierState result = instance.state();
        assertEquals(expResult, result);
    }

//    /**
//     * Test of meals method, of class Cashier.
//     */
//    @Test
//    public void testMeals() {
//        System.out.println("meals");
//        Cashier instance = new Cashier();
//        List<Meal> expResult = null;
//        List<Meal> result = instance.meals();
//        assertEquals(expResult, result);
//    }

//    /**
//     * Test of generateMBreference method, of class Cashier.
//     */
//    @Test
//    public void testGenerateMBreference() throws Exception {
//        System.out.println("generateMBreference");
//        double amount = 0.0;
//        Cashier instance = new Cashier();
//        String expResult = "";
//        String result = instance.generateMBreference(amount);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of confirmPayment method, of class Cashier.
//     */
//    @Test
//    public void testConfirmPayment() {
//        System.out.println("confirmPayment");
//        CafeteriaUser user = null;
//        double amount = 0.0;
//        Cashier instance = new Cashier();
//        instance.confirmPayment(user, amount);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of sameAs method, of class Cashier.
//     */
//    @Test
//    public void testSameAs() {
//        System.out.println("sameAs");
//        Object other = null;
//        Cashier instance = new Cashier();
//        boolean expResult = false;
//        boolean result = instance.sameAs(other);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of is method, of class Cashier.
//     */
//    @Test
//    public void testIs() {
//        System.out.println("is");
//        Long id = null;
//        Cashier instance = new Cashier();
//        boolean expResult = false;
//        boolean result = instance.is(id);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of id method, of class Cashier.
//     */
//    @Test
//    public void testId() {
//        System.out.println("id");
//        Cashier instance = new Cashier();
//        Long expResult = null;
//        Long result = instance.id();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of nameOfMealsServed method, of class Cashier.
//     */
//    @Test
//    public void testNameOfMealsServed() {
//        System.out.println("nameOfMealsServed");
//        Cashier instance = new Cashier();
//        List<String> expResult = null;
//        List<String> result = instance.nameOfMealsServed();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of countBookingDiffOfMeals method, of class Cashier.
//     */
//    @Test
//    public void testCountBookingDiffOfMeals() throws Exception {
//        System.out.println("countBookingDiffOfMeals");
//        Cashier instance = new Cashier();
//        List<Integer> expResult = null;
//        List<Integer> result = instance.countBookingDiffOfMeals();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of checkState method, of class Cashier.
     */
    @Test
    public void testCheckStateTrue() {
        System.out.println("checkState");
        CashierState state = CashierState.CLOSED;
        Cashier instance = new Cashier();
        boolean expResult = true;
        boolean result = instance.checkState(state);
        assertEquals(expResult, result);
    }
     /**
     * Test of checkState method, of class Cashier.
     */
    @Test
    public void testCheckStateFalse() {
        System.out.println("checkState");
        CashierState state = CashierState.OPEN;
        Cashier instance = new Cashier();
        boolean expResult = false;
        boolean result = instance.checkState(state);
        assertEquals(expResult, result);
    }
}
