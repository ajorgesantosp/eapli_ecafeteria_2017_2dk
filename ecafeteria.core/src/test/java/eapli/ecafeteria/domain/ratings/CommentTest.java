/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.ratings;

import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUserBuilder;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.meals.MealType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author 1150585 / 1150801
 */
public class CommentTest {

    private Comment c1;
    private Comment c2;
    private Comment c3;

    private MealRating mr1;
    private MealRating mr2;

    private Date date;

    private CafeteriaUser cu1;

    public CommentTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws ParseException {
        date = new SimpleDateFormat("dd/MM/yyyy").parse("17/05/2017");

        final Set<RoleType> roles = new HashSet<>();
        roles.add(RoleType.CAFETERIA_USER);

        cu1 = new CafeteriaUserBuilder()
                .withOrganicUnit(new OrganicUnit("dummy", "dummy", "dummy")).withMecanographicNumber("1234")
                .withSystemUser(new SystemUser("dummy", "duMMy1", "dummy", "dummy", "a@b.ro", roles)).build();

        mr1 = new MealRating(cu1, date, MealType.LUNCH, 1);
        mr2 = new MealRating(cu1, date, MealType.DINNER, 4);

        c1 = new Comment("Muito boa refeicao", mr1);
        c2 = new Comment("Muito boa refeicao", mr1);
        c3 = new Comment("Muito má refeicao", mr2);
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of idRating, of class Comment
     */
    @Test(expected = IllegalStateException.class)
    public void ensureIdRatingIsNotNull() {
        System.out.println("ensureIdRatingIsNotNull");
        Comment comTest = new Comment("Comment Test 12345", null);
    }

    /**
     * Test of comment, of class Comment
     */
    @Test(expected = IllegalStateException.class)
    public void ensureCommentIsNotNull() {
        System.out.println("ensureCommentIsNotNull");
        Comment comTest = new Comment(null, mr1);
    }

    /**
     * Test of equals method, of class Comment.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        boolean result1 = c1.equals(c2);
        boolean result2 = c1.equals(c3);
        assertTrue(result1);
        assertFalse(result2);
    }
}
