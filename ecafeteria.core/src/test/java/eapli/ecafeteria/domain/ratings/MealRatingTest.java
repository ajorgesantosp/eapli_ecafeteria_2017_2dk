package eapli.ecafeteria.domain.ratings;

import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUserBuilder;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.meals.MealType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author 1150585 / 1150801
 */
public class MealRatingTest {

    private CafeteriaUser aCafeteriaUser;
    private Date date;

    public MealRatingTest() {

    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws ParseException {
        date = new SimpleDateFormat("dd/MM/yyyy").parse("17/05/2017");

        final Set<RoleType> roles = new HashSet<>();
        roles.add(RoleType.CAFETERIA_USER);

        aCafeteriaUser = new CafeteriaUserBuilder()
                .withOrganicUnit(new OrganicUnit("dummy", "dummy", "dummy")).withMecanographicNumber("1234")
                .withSystemUser(new SystemUser("dummy", "duMMy1", "dummy", "dummy", "a@b.ro", roles)).build();

    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of user method, of class MealRating.
     */
    @Test(expected = IllegalStateException.class)
    public void ensureUserIsNotNull() {
        System.out.println("ensureUserIsNotNull");

        MealRating mrInstance = new MealRating(null, date, MealType.LUNCH, 1);
    }

    @Test(expected = IllegalStateException.class)
    public void ensureDateIsNotNull() {
        System.out.println("ensureDateIsNotNull");
        MealRating mrInstance = new MealRating(aCafeteriaUser, null, MealType.LUNCH, 1);

    }

    @Test(expected = IllegalStateException.class)
    public void ensureMealTypeIsNotNull() {
        System.out.println("ensureDateIsNotNull");
        MealRating mrInstance = new MealRating(aCafeteriaUser, date, null, 1);

    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureRatingIsPositive() {
        System.out.println("ensureDateIsNotNull");
        MealRating mrInstance = new MealRating(aCafeteriaUser, date, MealType.LUNCH, -1);

    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureRatingIsWithInBounds() {
        System.out.println("ensureDateIsNotNull");
        MealRating mrInstance = new MealRating(aCafeteriaUser, date, MealType.LUNCH, 6);

    }

    /**
     * Test of equals method, of class MealRating.
     */
    @Test
    public void ensureNoRatingsAreEqual() {
        System.out.println("ensureNoRatingsAreEqual");
        MealRating aMealRating = new MealRating(aCafeteriaUser, date, MealType.LUNCH, 1);
        MealRating anotherMealRating = new MealRating(aCafeteriaUser, date, MealType.LUNCH, 1);
        final boolean expected = aMealRating.equals(anotherMealRating);
        assertTrue(expected);
    }
}
