# *4 - Make Reservation*

## Short Format

+ User starts makes reservation. The system asks for the date and type of the reservation. The user chooses the date and type of reservation. The system shows the dishes of the day and night and allergens if there are. The user chooses the dish from the list of available dishes on that date. The system validates this reservation and asks if there will be more reservations. The user answers yes or no, if not, ends the session. The system stores the reservations made and informs the success of the operation.

## Short Format SSD

![Make Reservation](documentation\MakeReservation\MakeReservationSSD.png)

## Full Format

## Main Actor

+ User

## Stakeholders and their interests

+ User - Makes a reservation for a day that he wants to eat at the cantine.
+ Kitchen - Is a new reservation. This reservation obligated to add a new dish to number of dishes that must be prepared.

## Pre-conditions

+ User must be registed in system.
+ Must exist dishes to make the reservation.
+ Must be made before the date of reservation.

## Post-conditions

+ User make a reservation of a dish to a certain day.
+ A dish will be added in number of dishes to do in the certain day.

## Primary success scenario (or base flow)

1. User starts makes reservation.
2. The system asks for the date and type of the reservation.
3. The user chooses the date and type of reservation.
4. The system shows the dishes of the day and night and allergens if there are.
5. The user chooses the dish from the list of available dishes on that date.
6. The system validates this reservation and asks if there will be more reservations.
7. The user answers yes or no, if not, ends the session.
8. The system stores the reservations made and informs the success of the operation.
9. *Steps 2 to 6 are repeated as long as the user answers yes in step 7.*

## Extensions (or alternate flows)

> * User cancels
>> Use case ended.
> 3. User doesn't choose a available date or doesn't choose a available type of reservation.
>> System informs user that must be change his choice.
>>> User doesn't change.
>>>> Use case ended.
> 5. User doesn't choose a available dish
>> System informs user that must be choose a or another dish
>>> User doesn't change.
>>>> Use case ended.

## Special Requirements

+ Account card must have enough money to purchase the meal.
+ There is a period for the reservation of the meal to be made.

## List of Technologies and Data Variations  

+

## Frequency of Occurrence

 +

## Open questions

+ The user can cancel the reservation?
+ If contains a allergens user can choose the dish?
