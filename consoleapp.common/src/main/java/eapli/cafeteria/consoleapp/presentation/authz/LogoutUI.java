package eapli.cafeteria.consoleapp.presentation.authz;

import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.ecafeteria.application.authz.LogoutController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;


 /**
 *
 * @author 1150416
 */
public class LogoutUI extends AbstractUI{

    private final LogoutController theController = new LogoutController();
    
    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        if (this.theController.logout()) {
            System.out.println("Logout Successful");
            new ExitWithMessageAction().execute();
            System.exit(0);
            return true;
        } else {
            System.out.println("Logout Error");
            return false;
        }
    }

    @Override
    public String headline() {
        return "Logout";
    }
    
    
}
