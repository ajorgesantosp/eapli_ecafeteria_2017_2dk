/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.CardMovements;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.persistence.CardMovementsRepository;
import eapli.framework.persistence.repositories.TransactionalContext;
import eapli.framework.persistence.repositories.impl.jpa.JpaAutoTxRepository;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Esquilo
 */
public class JpaCardMovementsRepository extends JpaAutoTxRepository<CardMovements, MecanographicNumber>  implements CardMovementsRepository {
    
    
    
     public JpaCardMovementsRepository(TransactionalContext autoTx) {
        super(Application.settings().getPersistenceUnitName(), autoTx);
    }

 
 
  
     @Override
    public CardMovements findByMecanographicNumber(MecanographicNumber number) {
        Map<String, Object> params = new HashMap<>();
        params.put("number", number);
        return repo.matchOne("e.mecanographicNumber=:number", params);
    }
}
