/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.kitchen.KitchenAlertManagement;
import eapli.ecafeteria.persistence.AlertRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author 1150425 & 1150482
 */
public class JpaAlertRepository extends InMemoryRepository<KitchenAlertManagement, String> implements AlertRepository {

    @Override
    protected String newPK(KitchenAlertManagement entity) {
       return entity.alertName();
    }

    @Override
    public KitchenAlertManagement findByName(String name) {
        return matchOne(e -> e.alertName().equals(name));
    }
}
