/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.delivery.Cashier;
import eapli.ecafeteria.persistence.CashierRepository;

/**
 *
 * @author nunom
 */
public class JpaCashierRepository extends CafeteriaJpaRepositoryBase<Cashier, Long>
        implements CashierRepository {

    @Override
    public Cashier findById(Long id) {
        return matchOne("e.cashierID='"+ id+"'");
    }
}
