/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.BookingRepository;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author ismal
 */
public class JpaBookingRepository extends CafeteriaJpaRepositoryBase<Booking, Long> implements BookingRepository {

    @Override
    public Iterable<Booking> activeReserves() {
        List<Booking> lstBookings = new ArrayList<>();
        for (Booking b : this.findAll()) {
            lstBookings.add(b);
        }
        return lstBookings;
    }

    @Override
    public Booking findById(Long id) {
        String s = "e.id='" + id + "'";
        return matchOne(s);
    }

    @Override
    public Iterable<Booking> bookingReservedOfUserThisDayOn(CafeteriaUser user, Calendar date) {
        List<Booking> mealList = new ArrayList<>();
        List<Booking> listBookByUser;
        listBookByUser = getBookingsByUser(user);
        for (Booking booking : listBookByUser) {
            if (booking.state().equals(Booking.States.Reserved)) {
                if ((booking.meal().date().getTime().after(date.getTime())) || compareDates(booking.meal().date(), date)) {
                    mealList.add(booking);
                }
            }
        }

        return mealList;
    }

    @Override
    public List<Booking> getBookingsByState(Booking.States state) {
        Map<String, Object> params = new HashMap<>();
        params.put("state", state);

        return match("e.state=:state", params);
    }

    private List<Booking> getBookingsByUser(CafeteriaUser user) {
        Map<String, Object> params = new HashMap<>();
        params.put("user", user);

        return match("e.user=:user", params);
    }

    @Override
    public List<Booking> getBookingsBymealID(Meal m) {
        Map<String, Object> params = new HashMap<>();
        params.put("meal", m);

        return match("e.meal=:meal", params);
    }

    @Override
    public Booking reserveByUserStateMealID(CafeteriaUser user, Booking.States state, Meal meal) {
        List<Booking> listBookBymealID;
        List<Booking> listBookByUser;
        List<Booking> listBookByState;

        listBookByUser = getBookingsByUser(user);
        listBookByState = getBookingsByState(state);
        listBookBymealID = getBookingsBymealID(meal);

        for (Booking booking : listBookByUser) {
            for (Booking bookState : listBookByState) {
                for (Booking bookMealID : listBookBymealID) {
                    if (bookState.user().mecanographicNumber().equals(booking.user().mecanographicNumber()) && booking.meal().id().equals(bookMealID.meal().id())) {
                        return booking;
                    }
                }
            }
        }

        return null;
    }

    @Override
    public Booking checkIfBookingExists(CafeteriaUser user, Booking.States state, MealType mealType, Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        Booking finalBooking=null;
        
        Query q = entityManager().createQuery("Select DISTINCT (b) FROM Booking b, Meal m "
                + "Where b.user=:idUser "
                + "AND m.mealType=:mealTypeBooking "
                + "AND b.state=:state "
                + "AND m.date=:dateBooking");

        q.setParameter("idUser",user);
        q.setParameter("mealTypeBooking", mealType);
        q.setParameter("dateBooking", cal);
        q.setParameter("state", state);
        
        try{
        finalBooking=(Booking)q.getSingleResult();
            
        }catch(NoResultException ex){
            System.out.println("There are no bookings for the specified data"); 
            
        }

        
        return finalBooking;
    }

    @Override
    public Iterable<Booking> findBookingsByMealAndState(Booking.States state, Meal meal) {
        Map<String, Object> params = new HashMap<>();
        params.put("meal", meal);
        params.put("state", state);

        Iterable<Booking> bookings = match("e.meal=:meal and e.state=:state", params);
        ArrayList<Booking> bookingsList = new ArrayList<>();
        for (Booking b : bookings) {
            bookingsList.add(b);
        }
        return bookingsList;
    }

    @Override
    public Iterable<Booking> getBookingForNextDays(CafeteriaUser user, Date currentDay, Date limitDay) {

        final String idUser = user.id().getNumber();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = formatter.format(currentDay);
        String limitDayString = formatter.format(limitDay);
        Query b = entityManager().createQuery("SELECT b FROM Booking b, Meal m "
                + "Where b.user.mecanographicNumber.number=:idUser AND  b.meal.date>=:currentDate AND b.meal.date<=:limitDayString");

        b.setParameter("currentDate", currentDay);
        b.setParameter("limitDayString", limitDay);
        b.setParameter("idUser", idUser);
        Iterable<Booking> bookings = b.getResultList();
        ArrayList<Booking> bookingsList = new ArrayList<>();
        for (Booking booking : bookings) {
            bookingsList.add(booking);
        }
        return bookingsList;

    }

    public boolean compareDates(Calendar date1, Calendar date2) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String d1 = df.format(date1.getTime());
        String d2 = df.format(date2.getTime());
        if (d1.equals(d2)) {
            return true;
        }
        return false;
    }

}
