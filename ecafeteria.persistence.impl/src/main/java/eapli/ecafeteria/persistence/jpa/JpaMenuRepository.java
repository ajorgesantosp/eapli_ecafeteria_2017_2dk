/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.domain.meals.MenuState;
import eapli.ecafeteria.persistence.MenuRepository;
import java.util.Calendar;
import javax.persistence.TypedQuery;

/**
 *
 * @author Leandro Santos
 */
public class JpaMenuRepository extends CafeteriaJpaRepositoryBase<Menu, Long> implements MenuRepository {

    @Override
    public Menu findByDate(Calendar date) {
        return matchOne("e.date=:date", "date", date);
    }

    @Override
    public Iterable<Menu> findMenuInProgress() {
        TypedQuery<Menu> query = entityManager().createQuery("SELECT m from Menu m where m.menuState=:state", Menu.class);
        query.setParameter("state", MenuState.IN_PROGRESS);
        return query.getResultList();
    }
    
    @Override
    public Iterable<Menu> findMenuPublished() {
        TypedQuery<Menu> query = entityManager().createQuery("SELECT m from Menu m where m.menuState=:state", Menu.class);
        query.setParameter("state", MenuState.PUBLISHED);
        return query.getResultList();
    }

    @Override
    public Menu findByStartFinishDate(Calendar startDate, Calendar finishDate) {
        return matchOne("e.startDate=:startDate", "startDate", startDate, "e.finishDate=:finishDate", "finishDate", finishDate);
    }
    
    @Override
    public Menu menuByDate(Calendar date) {
        return matchOne("e.startDate<=:date", "date", date, "e.finishDate>=:date", "date", date);
    }

    @Override
    public Menu findByStartDate(Calendar startDate) {
        return  matchOne("e.startDate=:startDate", "startDate", startDate);
    }

    @Override
    public Menu findByFinishDate(Calendar finishDate) {
         return matchOne("e.finishDate=:finishDate", "finishDate", finishDate);
    }

}
