package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.persistence.BatchRepository;
import java.util.HashMap;
import java.util.Map;

public class JpaBatchRepository extends CafeteriaJpaRepositoryBase<Batch, Long> implements BatchRepository {

    @Override
    public Batch findByAcronym(String acronym) {
        Map<String, Object> params = new HashMap<>();
        params.put("acronym", acronym);

        return matchOne("e.acronym=:acronym", params);
    }
}
