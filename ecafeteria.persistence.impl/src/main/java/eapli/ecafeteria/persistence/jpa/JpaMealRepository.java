/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;
import java.util.ArrayList;
import java.util.Calendar;
import javax.persistence.Query;

/**
 *
 * Created by 1150450 and 1150444 on 04/05/2017.
 */
public class JpaMealRepository extends CafeteriaJpaRepositoryBase<Meal, Long> implements MealRepository {

    @Override
    public Meal findByID(Long id) {
        return matchOne("e.mealID=:id", "id", id);
    }

    @Override
    public Iterable<Meal> mealsByDateAndType(Calendar date, MealType type) {
        Query query = entityManager().createQuery("SELECT DISTINCT m FROM Meal m "
                + "WHERE m.date=:date AND m.mealType=:type", Meal.class);
        query.setParameter("date", date);
        query.setParameter("type", type);
        ArrayList<Meal> listMeal=new ArrayList<>();
        for (Object m: query.getResultList()) {
            listMeal.add((Meal)m);
        }
        return listMeal;
 
    }

    @Override
    public Iterable<Meal> duplicateMeals(Meal receivedMeal) {
        Query query = entityManager().createQuery("SELECT DISTINCT m FROM Meal m "
                + "WHERE m.date=:date AND m.mealType=:type AND m.dish=:dish", Meal.class);
        query.setParameter("date", receivedMeal.date());
        query.setParameter("type", receivedMeal.mealType());
        query.setParameter("dish", receivedMeal.dish());

        return query.getResultList();
    }

    @Override
    public Iterable<Meal> mealsByDate(Calendar date) {
        Query query = entityManager().createQuery("SELECT DISTINCT m FROM Meal m WHERE m.date=:date", Meal.class);
        query.setParameter("date", date);

        return query.getResultList();
    }

    @Override
    public Iterable<Meal> allMealsDateInterval(Calendar startDate, Calendar finishDate) {
        Query query = entityManager().createQuery("SELECT DISTINCT m FROM Meal m "
                + "WHERE m.date>=:date1 AND m.date<=:date2", Meal.class);

        query.setParameter("date1", startDate);
        query.setParameter("date2", finishDate);

        return query.getResultList();
    }

}
