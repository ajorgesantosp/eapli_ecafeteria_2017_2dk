package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.ratings.Comment;
import eapli.ecafeteria.persistence.CommentRepository;

/**
 *
 * @author 1150585 / 1150801
 */
public class JpaCommentRepository extends CafeteriaJpaRepositoryBase<Comment, Long> implements CommentRepository {

    
}
