package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.ratings.Comment;
import eapli.ecafeteria.persistence.CommentRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author 1150585 / 1150801
 */
public class InMemoryCommentRepository extends InMemoryRepository<Comment, Long> implements CommentRepository {

    @Override
    protected Long newPK(Comment entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
