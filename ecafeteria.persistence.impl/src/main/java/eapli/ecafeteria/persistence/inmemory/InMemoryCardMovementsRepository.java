/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.authz.CardMovements;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.persistence.CardMovementsRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 *
 * @author Esquilo
 */
public class InMemoryCardMovementsRepository extends InMemoryRepository<CardMovements, MecanographicNumber>
	implements CardMovementsRepository {

    

    

    @Override
    public CardMovements findByMecanographicNumber(MecanographicNumber number) {
	return data().get(number);
    }

    @Override
    protected MecanographicNumber newPK(CardMovements entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void forEach(Consumer<? super CardMovements> action) {
        super.forEach(action); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Spliterator<CardMovements> spliterator() {
        return super.spliterator(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
