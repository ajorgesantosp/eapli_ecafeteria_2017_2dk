package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.ratings.MealRating;
import eapli.ecafeteria.persistence.MealRatingRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author 1150585 / 1150801
 */
public class InMemoryMealRatingRepository extends InMemoryRepository<MealRating, Long> implements MealRatingRepository {

    @Override
    public Iterable<MealRating> activeMealRatings() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Long newPK(MealRating entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
}
