/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.function.Predicate;

/**
 *
 * @author ismal
 */
public class InMemoryBookingRepository extends InMemoryRepository<Booking, Long>
        implements BookingRepository {

    @Override
    protected Long newPK(Booking entity) {
        return entity.id();
    }

    @Override
    public Iterable<Booking> activeReserves() {
        return this.findAll();
    }

    @Override
    public Booking findById(Long id) {
        return this.findOne(id).get();
    }

    @Override
    public Iterable<Booking> bookingReservedOfUserThisDayOn(CafeteriaUser user, Calendar date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Booking reserveByUserStateMealID(CafeteriaUser user, Booking.States state, Meal meal) {
        return this.reserveByUserStateMealID(user, state, meal);
    }

    @Override
    public Booking checkIfBookingExists(CafeteriaUser user, Booking.States state, MealType mealType, Date date) {
        return this.checkIfBookingExists(user, state, mealType, date);
    }

    @Override
    public Iterable<Booking> getBookingsByState(Booking.States state) {
        return match(e->e.state().equals(state));
    }

    @Override
    public Iterable<Booking> getBookingsBymealID(Meal m) {
        return match(e->e.meal().equals(m));
    }

    @Override
    public Iterable<Booking> findBookingsByMealAndState(Booking.States state, Meal meal) {
        return match(e->e.state().equals(state) && e.meal().equals(meal));
    }
    
        @Override
    public Iterable<Booking> getBookingForNextDays(CafeteriaUser user, Date currentDay, Date limitDay) {
        return match(e -> e.id().equals(user) && e.meal().date().after(currentDay) && e.meal().date().before(limitDay));
    }

}
