package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.delivery.Cashier;
import eapli.ecafeteria.persistence.CashierRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nunom
 */
public class InMemoryCashierRepository extends InMemoryRepositoryWithLongPK<Cashier>
        implements CashierRepository {
    
    @Override
    public Cashier findById(Long id) {
        return matchOne(e -> e.id().equals(id));
    }
}

