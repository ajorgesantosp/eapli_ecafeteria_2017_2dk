/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Created by 1150450 and 1150444 on 04/05/2017.
 */
public class InMemoryMealRepository extends InMemoryRepository<Meal, Long> implements MealRepository {

    @Override
    protected Long newPK(Meal entity) {
        return entity.id();
    }

    @Override
    public Meal findByID(Long id) {
        return matchOne(e -> e.id().equals(id));
    }

    @Override
    public Iterable<Meal> mealsByDateAndType(Calendar date, MealType type) {
        return (List<Meal>) match(e -> (e.sameAs(type) && e.sameAs(date)));
    }

    @Override
    public Iterable<Meal> duplicateMeals(Meal receivedMeal) {
        return match(e -> (e.mealType().equals(receivedMeal.mealType()) && e.date().equals(receivedMeal.date()) && e.dish().sameAs(receivedMeal.dish())));
    }

    @Override
    public Iterable<Meal> mealsByDate(Calendar date) {
        return match(e -> e.date().equals(date));
    }

    @Override
    public Iterable<Meal> allMealsDateInterval(Calendar startDate, Calendar finishDate) {
        return match(e -> (e.date().after(startDate) || e.date().equals(startDate) || e.date().before(finishDate) || e.date().equals(finishDate)));
    }

}
