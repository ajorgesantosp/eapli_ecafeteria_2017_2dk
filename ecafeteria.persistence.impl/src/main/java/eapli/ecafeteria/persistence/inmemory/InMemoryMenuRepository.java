package eapli.ecafeteria.persistence.inmemory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Calendar;

/**
 *
 * @author Leandro Santos
 */
public class InMemoryMenuRepository extends InMemoryRepository<Menu, Long> implements MenuRepository {

    @Override
    protected Long newPK(Menu entity) {
        return entity.id();
    }

    @Override
    public Menu findByDate(Calendar startDate) {
        return matchOne(e -> e.startDate().equals(startDate));
    }

    @Override
    public Menu findByStartFinishDate(Calendar startDate, Calendar finishDate) {
        return matchOne(e -> e.startDate().equals(startDate) && e.finishDate().equals(finishDate));
    }

    @Override
    public Iterable<Menu> findMenuInProgress() {
        return match(e -> e.isInProgress());
    }

    @Override
    public Iterable<Menu> findMenuPublished() {
        return match(e -> !e.isInProgress());
    }

    @Override
    public Menu menuByDate(Calendar date) {
        return matchOne(e -> e.startDate().before(date) && e.finishDate().after(date));
    }

    @Override
    public Menu findByStartDate(Calendar startDate) {
       return matchOne(e -> e.startDate().equals(startDate));
    }

    @Override
    public Menu findByFinishDate(Calendar finishDate) {
       return  matchOne(e -> e.finishDate().equals(finishDate));
    }

}
