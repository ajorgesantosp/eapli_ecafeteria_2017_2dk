package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.booking.Booking;
import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.persistence.BatchRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

public class InMemoryBatchRepository extends InMemoryRepository<Batch, Long> implements BatchRepository {

    @Override
    protected Long newPK(Batch entity) {
        return entity.id();
    }

    @Override
    public Batch findByAcronym(String acronym) {
        return matchOne(e -> e.acronym().equals(acronym));
    }
}
